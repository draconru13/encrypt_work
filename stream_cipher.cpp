#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <cmath>
#include<string> 

#include <typeinfo>

using namespace std;

string bit_1_encrypt_1(register int x) {
	register string test_1;
	stringstream test;

	string bit_test_1 = "";
	register long long remainder, i = 1;
	register long long binaryNumber1 = 0;

	while (x != 0) {
		remainder = x % 2;
		x /= 2;
		binaryNumber1 += remainder * i;
		i *= 10;
	}
	test << binaryNumber1;
	test >> bit_test_1;
	
	if (size(bit_test_1) < 7) {
		for (register int i = 0; i < (7- size(bit_test_1)); i++) {
			bit_test_1 = '0' + bit_test_1;
		}
	}
	
	
	return bit_test_1;
}

string bit_2_encrypt_1(register int x1) {
	register string test_1;
	stringstream test;

	string bit_test_2 = "";
	register long long remainder, i = 1;
	register long long binaryNumber1 = 0;

	while (x1 != 0) {
		remainder = x1 % 2;
		x1 /= 2;
		binaryNumber1 += remainder * i;
		i *= 10;
	}
	test << binaryNumber1;
	test >> bit_test_2;

	if (size(bit_test_2) < 7) {
		for (register int i = 0; i < (7 - size(bit_test_2)); i++) {
			bit_test_2 = '0' + bit_test_2;
		}
	}
	return bit_test_2;
}


string bit_3_encrypt_1(register int x2) {
	register string test_1;
	stringstream test;

	string bit_test_3 = "";
	register long long remainder, i = 1;
	register long long binaryNumber1 = 0;

	while (x2 != 0) {
		remainder = x2 % 2;
		x2 /= 2;
		binaryNumber1 += remainder * i;
		i *= 10;
	}
	test << binaryNumber1;
	test >> bit_test_3;

	if (size(bit_test_3) < 7) {
		for (register int i = 0; i < (7 - size(bit_test_3)); i++) {
			bit_test_3 = '0' + bit_test_3;
		}
	}
	return bit_test_3;
}

int decimal_Number(register string a, register int counter_str) {

	register long long int  bitstream_int;
	stringstream ss;
	ss << a;
	ss >> bitstream_int;

	register long long decimalNumber = 0, i = 0, remainder;
	while (bitstream_int != 0) {
		remainder = bitstream_int % 10;
		bitstream_int /= 10;
		decimalNumber += remainder * pow(2, i);
		++i;
	}

	return decimalNumber;
}

string encrypt_1() {
	register string number1;
	register string number2;
	register string word;

	register string test_1;
	stringstream test;

	register string test_2;
	stringstream test1;

	register string test_3;
	stringstream test2;

tryAgain_1:
	cout << "\n                   Введите первый ключ, длина которого больше 2-х сиволов и не больше 10 символов: \n";
	getline(cin, number1, ';');
	if (size(number1) > 10 || size(number1) < 2) {
		cout << "\nВведите другой набор ключей: \n";
		goto tryAgain_1;
	}

tryAgain_2:
	cout << "\n                   Введите второй ключ, длина которого больше 2-х сиволов и не больше 10 символов: \n";
	getline(cin, number2, ';');
	if (size(number2) > 10 || size(number2) < 2) {
		cout << "\nВведите другой набор ключей: \n";
		goto tryAgain_2;
	}


	// перевод в бинарник первого ключа
	register int counter_1  = 1;
	string bit_test_1 = "";
	for (counter_1; counter_1 <= size(number1) - 1; counter_1++) {
		register int x = (int)(number1[counter_1]);
		bit_test_1 += bit_1_encrypt_1(x);
	}
	cout << "\nБинарный код первого ключа: " << bit_test_1 << "\n";

	// перевод в бинарник второго ключа
	register int counter_2 = 1;
	string bit_test_2 = "";
	for (counter_2; counter_2 <= size(number2) - 1; counter_2++) {
		register int x1 = (int)(number2[counter_2]);
		bit_test_2 += bit_2_encrypt_1(x1);
	}
	cout << "\nБинарный код второго ключа: " << bit_test_2 << "\n";


	cout << "\n                   Введите слово, которое требуется зашифровать: \n";
	getline(cin, word, ';');

	// перевод в бинарник шифруемого слова
	register int counter_3 = 1;
	string bit_test_3 = "";
	for (counter_3; counter_3 <= size(word) - 1; counter_3++) {
		register int x2 = (int)(word[counter_3]);
		bit_test_3 += bit_3_encrypt_1(x2);
	}
	cout << "\nБинарный код введённого слова:      " << bit_test_3 << "\n";

	register string beatstream;
	register char null_element1;
	register char null_element2;

	//цикл для создания битового потока, который будет шифровать 
	for (register int counter = 0; counter < size(bit_test_3); counter++) {
		register string beatstream_bit;
		stringstream ss;

		switch (size(bit_test_1))
		{

		case 2:
		{
			if (bit_test_1[0] == '1' && bit_test_1[1] == '1' || bit_test_1[0] == '0' && bit_test_1[1] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 3:
		{
			if (bit_test_1[1] == '1' && bit_test_1[2] == '1' || bit_test_1[1] == '0' && bit_test_1[2] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 4:
		{
			if (bit_test_1[3] == '1' && bit_test_1[2] == '1' || bit_test_1[3] == '0' && bit_test_1[2] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 5:
		{
			if (bit_test_1[2] == '1' && bit_test_1[4] == '1' || bit_test_1[2] == '0' && bit_test_1[4] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 6:
		{
			if (bit_test_1[5] == '1' && bit_test_1[4] == '1' || bit_test_1[5] == '0' && bit_test_1[4] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 7:
		{
			if (bit_test_1[3] == '1' && bit_test_1[6] == '1' || bit_test_1[3] == '0' && bit_test_1[6] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 8:
		{
			if (bit_test_1[3] == '0' && bit_test_1[4] == '0' && bit_test_1[5] == '0' && bit_test_1[7] == '0' || bit_test_1[3] == '0' && bit_test_1[4] == '0' && bit_test_1[5] == '1' && bit_test_1[7] == '1' || bit_test_1[3] == '0' && bit_test_1[4] == '1' && bit_test_1[5] == '0' && bit_test_1[7] == '1' || bit_test_1[3] == '0' && bit_test_1[4] == '1' && bit_test_1[5] == '1' && bit_test_1[7] == '0' || bit_test_1[3] == '1' && bit_test_1[4] == '0' && bit_test_1[5] == '0' && bit_test_1[7] == '1' || bit_test_1[3] == '1' && bit_test_1[4] == '0' && bit_test_1[5] == '1' && bit_test_1[7] == '0' || bit_test_1[3] == '1' && bit_test_1[4] == '1' && bit_test_1[5] == '0' && bit_test_1[7] == '0' || bit_test_1[3] == '1' && bit_test_1[4] == '1' && bit_test_1[5] == '1' && bit_test_1[7] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 9:
		{
			if (bit_test_1[4] == '1' && bit_test_1[8] == '1' || bit_test_1[4] == '0' && bit_test_1[8] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 10:
		{
			if (bit_test_1[6] == '1' && bit_test_1[9] == '1' || bit_test_1[6] == '0' && bit_test_1[9] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 11:
		{
			if (bit_test_1[8] == '1' && bit_test_1[10] == '1' || bit_test_1[8] == '0' && bit_test_1[10] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 12:
		{
			if (bit_test_1[5] == '0' && bit_test_1[7] == '0' && bit_test_1[10] == '0' && bit_test_1[11] == '0' || bit_test_1[5] == '0' && bit_test_1[7] == '0' && bit_test_1[10] == '1' && bit_test_1[11] == '1' || bit_test_1[5] == '0' && bit_test_1[7] == '1' && bit_test_1[10] == '0' && bit_test_1[11] == '1' || bit_test_1[5] == '0' && bit_test_1[7] == '1' && bit_test_1[10] == '1' && bit_test_1[11] == '0' || bit_test_1[5] == '1' && bit_test_1[7] == '0' && bit_test_1[10] == '0' && bit_test_1[11] == '1' || bit_test_1[5] == '1' && bit_test_1[7] == '0' && bit_test_1[10] == '1' && bit_test_1[11] == '0' || bit_test_1[5] == '1' && bit_test_1[7] == '1' && bit_test_1[10] == '0' && bit_test_1[11] == '0' || bit_test_1[5] == '1' && bit_test_1[7] == '1' && bit_test_1[10] == '1' && bit_test_1[11] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 13:
		{
			if (bit_test_1[8] == '0' && bit_test_1[9] == '0' && bit_test_1[11] == '0' && bit_test_1[12] == '0' || bit_test_1[8] == '0' && bit_test_1[9] == '0' && bit_test_1[11] == '1' && bit_test_1[12] == '1' || bit_test_1[8] == '0' && bit_test_1[9] == '1' && bit_test_1[11] == '0' && bit_test_1[12] == '1' || bit_test_1[8] == '0' && bit_test_1[9] == '1' && bit_test_1[11] == '1' && bit_test_1[12] == '0' || bit_test_1[8] == '1' && bit_test_1[9] == '0' && bit_test_1[11] == '0' && bit_test_1[12] == '1' || bit_test_1[8] == '1' && bit_test_1[9] == '0' && bit_test_1[11] == '1' && bit_test_1[12] == '0' || bit_test_1[8] == '1' && bit_test_1[9] == '1' && bit_test_1[11] == '0' && bit_test_1[12] == '0' || bit_test_1[8] == '1' && bit_test_1[9] == '1' && bit_test_1[11] == '1' && bit_test_1[12] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 14:
		{

			if (bit_test_1[3] == '0' && bit_test_1[7] == '0' && bit_test_1[12] == '0' && bit_test_1[13] == '0' || bit_test_1[3] == '0' && bit_test_1[7] == '0' && bit_test_1[12] == '1' && bit_test_1[13] == '1' || bit_test_1[3] == '0' && bit_test_1[7] == '1' && bit_test_1[12] == '0' && bit_test_1[13] == '1' || bit_test_1[3] == '0' && bit_test_1[7] == '1' && bit_test_1[12] == '1' && bit_test_1[13] == '0' || bit_test_1[3] == '1' && bit_test_1[7] == '0' && bit_test_1[12] == '0' && bit_test_1[13] == '1' || bit_test_1[3] == '1' && bit_test_1[7] == '0' && bit_test_1[12] == '1' && bit_test_1[13] == '0' || bit_test_1[3] == '1' && bit_test_1[7] == '1' && bit_test_1[12] == '0' && bit_test_1[13] == '0' || bit_test_1[3] == '1' && bit_test_1[7] == '1' && bit_test_1[12] == '1' && bit_test_1[13] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 15:
		{
			if (bit_test_1[14] == '1' && bit_test_1[13] == '1' || bit_test_1[14] == '0' && bit_test_1[13] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 16:
		{
			if (bit_test_1[3] == '0' && bit_test_1[14] == '0' && bit_test_1[12] == '0' && bit_test_1[15] == '0' || bit_test_1[3] == '0' && bit_test_1[14] == '0' && bit_test_1[12] == '1' && bit_test_1[15] == '1' || bit_test_1[3] == '0' && bit_test_1[14] == '1' && bit_test_1[12] == '0' && bit_test_1[15] == '1' || bit_test_1[3] == '0' && bit_test_1[14] == '1' && bit_test_1[12] == '1' && bit_test_1[15] == '0' || bit_test_1[3] == '1' && bit_test_1[14] == '0' && bit_test_1[12] == '0' && bit_test_1[15] == '1' || bit_test_1[3] == '1' && bit_test_1[14] == '0' && bit_test_1[12] == '1' && bit_test_1[15] == '0' || bit_test_1[3] == '1' && bit_test_1[14] == '1' && bit_test_1[12] == '0' && bit_test_1[15] == '0' || bit_test_1[3] == '1' && bit_test_1[14] == '1' && bit_test_1[12] == '1' && bit_test_1[15] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 17:
		{
			if (bit_test_1[16] == '1' && bit_test_1[13] == '1' || bit_test_1[16] == '0' && bit_test_1[13] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 18:
		{
			if (bit_test_1[10] == '1' && bit_test_1[17] == '1' || bit_test_1[10] == '0' && bit_test_1[17] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 19:
		{
			if (bit_test_1[13] == '0' && bit_test_1[16] == '0' && bit_test_1[17] == '0' && bit_test_1[18] == '0' || bit_test_1[13] == '0' && bit_test_1[16] == '0' && bit_test_1[17] == '1' && bit_test_1[18] == '1' || bit_test_1[13] == '0' && bit_test_1[16] == '1' && bit_test_1[17] == '0' && bit_test_1[18] == '1' || bit_test_1[13] == '0' && bit_test_1[16] == '1' && bit_test_1[17] == '1' && bit_test_1[18] == '0' || bit_test_1[13] == '1' && bit_test_1[16] == '0' && bit_test_1[17] == '0' && bit_test_1[18] == '1' || bit_test_1[13] == '1' && bit_test_1[16] == '0' && bit_test_1[17] == '1' && bit_test_1[18] == '0' || bit_test_1[13] == '1' && bit_test_1[16] == '1' && bit_test_1[17] == '0' && bit_test_1[18] == '0' || bit_test_1[13] == '1' && bit_test_1[16] == '1' && bit_test_1[17] == '1' && bit_test_1[18] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 20:
		{
			if (bit_test_1[16] == '1' && bit_test_1[19] == '1' || bit_test_1[16] == '0' && bit_test_1[19] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 21:
		{
			if (bit_test_1[18] == '1' && bit_test_1[20] == '1' || bit_test_1[18] == '0' && bit_test_1[20] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 22:
		{
			if (bit_test_1[21] == '1' && bit_test_1[20] == '1' || bit_test_1[21] == '0' && bit_test_1[20] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 23:
		{
			if (bit_test_1[17] == '1' && bit_test_1[22] == '1' || bit_test_1[17] == '0' && bit_test_1[22] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 24:
		{
			if (bit_test_1[21] == '0' && bit_test_1[16] == '0' && bit_test_1[22] == '0' && bit_test_1[23] == '0' || bit_test_1[21] == '0' && bit_test_1[16] == '0' && bit_test_1[22] == '1' && bit_test_1[23] == '1' || bit_test_1[21] == '0' && bit_test_1[16] == '1' && bit_test_1[22] == '0' && bit_test_1[23] == '1' || bit_test_1[21] == '0' && bit_test_1[16] == '1' && bit_test_1[22] == '1' && bit_test_1[23] == '0' || bit_test_1[16] == '1' && bit_test_1[21] == '0' && bit_test_1[22] == '0' && bit_test_1[23] == '1' || bit_test_1[21] == '1' && bit_test_1[16] == '0' && bit_test_1[22] == '1' && bit_test_1[23] == '0' || bit_test_1[21] == '1' && bit_test_1[16] == '1' && bit_test_1[22] == '0' && bit_test_1[23] == '0' || bit_test_1[21] == '1' && bit_test_1[16] == '1' && bit_test_1[22] == '1' && bit_test_1[23] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 25:
		{
			if (bit_test_1[21] == '1' && bit_test_1[24] == '1' || bit_test_1[21] == '0' && bit_test_1[24] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 26:
		{
			if (bit_test_1[19] == '0' && bit_test_1[24] == '0' && bit_test_1[25] == '0' && bit_test_1[23] == '0' || bit_test_1[24] == '0' && bit_test_1[19] == '0' && bit_test_1[25] == '1' && bit_test_1[23] == '1' || bit_test_1[24] == '0' && bit_test_1[19] == '1' && bit_test_1[25] == '0' && bit_test_1[23] == '1' || bit_test_1[24] == '0' && bit_test_1[19] == '1' && bit_test_1[25] == '1' && bit_test_1[23] == '0' || bit_test_1[19] == '1' && bit_test_1[24] == '0' && bit_test_1[25] == '0' && bit_test_1[23] == '1' || bit_test_1[24] == '1' && bit_test_1[19] == '0' && bit_test_1[25] == '1' && bit_test_1[23] == '0' || bit_test_1[24] == '1' && bit_test_1[19] == '1' && bit_test_1[25] == '0' && bit_test_1[23] == '0' || bit_test_1[24] == '1' && bit_test_1[19] == '1' && bit_test_1[25] == '1' && bit_test_1[23] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 27:
		{
			if (bit_test_1[21] == '0' && bit_test_1[24] == '0' && bit_test_1[25] == '0' && bit_test_1[26] == '0' || bit_test_1[24] == '0' && bit_test_1[21] == '0' && bit_test_1[25] == '1' && bit_test_1[26] == '1' || bit_test_1[24] == '0' && bit_test_1[21] == '1' && bit_test_1[25] == '0' && bit_test_1[26] == '1' || bit_test_1[24] == '0' && bit_test_1[21] == '1' && bit_test_1[25] == '1' && bit_test_1[26] == '0' || bit_test_1[21] == '1' && bit_test_1[24] == '0' && bit_test_1[25] == '0' && bit_test_1[26] == '1' || bit_test_1[24] == '1' && bit_test_1[21] == '0' && bit_test_1[25] == '1' && bit_test_1[26] == '0' || bit_test_1[24] == '1' && bit_test_1[21] == '1' && bit_test_1[25] == '0' && bit_test_1[26] == '0' || bit_test_1[24] == '1' && bit_test_1[21] == '1' && bit_test_1[25] == '1' && bit_test_1[26] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 28:
		{
			if (bit_test_1[27] == '1' && bit_test_1[24] == '1' || bit_test_1[27] == '0' && bit_test_1[24] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 29:
		{
			if (bit_test_1[26] == '1' && bit_test_1[28] == '1' || bit_test_1[26] == '0' && bit_test_1[28] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 30:
		{
			if (bit_test_1[6] == '0' && bit_test_1[27] == '0' && bit_test_1[28] == '0' && bit_test_1[29] == '0' || bit_test_1[6] == '0' && bit_test_1[27] == '0' && bit_test_1[28] == '1' && bit_test_1[29] == '1' || bit_test_1[6] == '0' && bit_test_1[27] == '1' && bit_test_1[28] == '0' && bit_test_1[29] == '1' || bit_test_1[6] == '0' && bit_test_1[27] == '1' && bit_test_1[28] == '1' && bit_test_1[29] == '0' || bit_test_1[6] == '1' && bit_test_1[27] == '0' && bit_test_1[28] == '0' && bit_test_1[29] == '1' || bit_test_1[6] == '1' && bit_test_1[27] == '0' && bit_test_1[28] == '1' && bit_test_1[29] == '0' || bit_test_1[6] == '1' && bit_test_1[27] == '1' && bit_test_1[28] == '0' && bit_test_1[29] == '0' || bit_test_1[6] == '1' && bit_test_1[27] == '1' && bit_test_1[28] == '1' && bit_test_1[29] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 31:
		{
			if (bit_test_1[27] == '1' && bit_test_1[30] == '1' || bit_test_1[27] == '0' && bit_test_1[30] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 32:
		{
			if (bit_test_1[9] == '0' && bit_test_1[30] == '0' && bit_test_1[31] == '0' && bit_test_1[29] == '0' || bit_test_1[9] == '0' && bit_test_1[30] == '0' && bit_test_1[31] == '1' && bit_test_1[29] == '1' || bit_test_1[9] == '0' && bit_test_1[31] == '1' && bit_test_1[30] == '0' && bit_test_1[29] == '1' || bit_test_1[9] == '0' && bit_test_1[30] == '1' && bit_test_1[31] == '1' && bit_test_1[29] == '0' || bit_test_1[9] == '1' && bit_test_1[30] == '0' && bit_test_1[31] == '0' && bit_test_1[29] == '1' || bit_test_1[9] == '1' && bit_test_1[30] == '0' && bit_test_1[31] == '1' && bit_test_1[29] == '0' || bit_test_1[9] == '1' && bit_test_1[30] == '1' && bit_test_1[31] == '0' && bit_test_1[29] == '0' || bit_test_1[9] == '1' && bit_test_1[30] == '1' && bit_test_1[31] == '1' && bit_test_1[29] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 33:
		{
			if (bit_test_1[19] == '1' && bit_test_1[32] == '1' || bit_test_1[19] == '0' && bit_test_1[32] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 34:
		{
			if (bit_test_1[29] == '0' && bit_test_1[30] == '0' && bit_test_1[33] == '0' && bit_test_1[25] == '0' || bit_test_1[25] == '0' && bit_test_1[30] == '0' && bit_test_1[33] == '1' && bit_test_1[29] == '1' || bit_test_1[25] == '0' && bit_test_1[29] == '1' && bit_test_1[30] == '0' && bit_test_1[33] == '1' || bit_test_1[25] == '0' && bit_test_1[30] == '1' && bit_test_1[33] == '1' && bit_test_1[29] == '0' || bit_test_1[25] == '1' && bit_test_1[30] == '0' && bit_test_1[33] == '0' && bit_test_1[29] == '1' || bit_test_1[25] == '1' && bit_test_1[30] == '0' && bit_test_1[33] == '1' && bit_test_1[29] == '0' || bit_test_1[25] == '1' && bit_test_1[30] == '1' && bit_test_1[33] == '0' && bit_test_1[29] == '0' || bit_test_1[25] == '1' && bit_test_1[30] == '1' && bit_test_1[33] == '1' && bit_test_1[29] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 35:
		{
			if (bit_test_1[34] == '1' && bit_test_1[32] == '1' || bit_test_1[34] == '0' && bit_test_1[32] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 36:
		{
			if (bit_test_1[24] == '1' && bit_test_1[35] == '1' || bit_test_1[24] == '0' && bit_test_1[35] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 37:
		{
			if (bit_test_1[32] == '0' && bit_test_1[30] == '0' && bit_test_1[36] == '0' && bit_test_1[35] == '0' || bit_test_1[30] == '0' && bit_test_1[32] == '0' && bit_test_1[35] == '1' && bit_test_1[36] == '1' || bit_test_1[30] == '0' && bit_test_1[32] == '1' && bit_test_1[35] == '0' && bit_test_1[36] == '1' || bit_test_1[30] == '0' && bit_test_1[32] == '1' && bit_test_1[35] == '1' && bit_test_1[36] == '0' || bit_test_1[30] == '1' && bit_test_1[32] == '0' && bit_test_1[35] == '0' && bit_test_1[36] == '1' || bit_test_1[30] == '1' && bit_test_1[32] == '0' && bit_test_1[35] == '1' && bit_test_1[36] == '0' || bit_test_1[30] == '1' && bit_test_1[32] == '1' && bit_test_1[35] == '0' && bit_test_1[36] == '0' || bit_test_1[30] == '1' && bit_test_1[32] == '1' && bit_test_1[35] == '1' && bit_test_1[36] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 38:
		{
			if (bit_test_1[32] == '0' && bit_test_1[31] == '0' && bit_test_1[36] == '0' && bit_test_1[37] == '0' || bit_test_1[31] == '0' && bit_test_1[32] == '0' && bit_test_1[37] == '1' && bit_test_1[36] == '1' || bit_test_1[31] == '0' && bit_test_1[32] == '1' && bit_test_1[37] == '0' && bit_test_1[36] == '1' || bit_test_1[31] == '0' && bit_test_1[32] == '1' && bit_test_1[37] == '1' && bit_test_1[36] == '0' || bit_test_1[31] == '1' && bit_test_1[32] == '0' && bit_test_1[37] == '0' && bit_test_1[36] == '1' || bit_test_1[31] == '1' && bit_test_1[32] == '0' && bit_test_1[37] == '1' && bit_test_1[36] == '0' || bit_test_1[31] == '1' && bit_test_1[32] == '1' && bit_test_1[37] == '0' && bit_test_1[36] == '0' || bit_test_1[31] == '1' && bit_test_1[32] == '1' && bit_test_1[37] == '1' && bit_test_1[36] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 39:
		{
			if (bit_test_1[34] == '1' && bit_test_1[38] == '1' || bit_test_1[34] == '0' && bit_test_1[38] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 40:
		{
			if (bit_test_1[34] == '0' && bit_test_1[35] == '0' && bit_test_1[36] == '0' && bit_test_1[39] == '0' || bit_test_1[34] == '0' && bit_test_1[35] == '0' && bit_test_1[39] == '1' && bit_test_1[36] == '1' || bit_test_1[34] == '0' && bit_test_1[35] == '1' && bit_test_1[39] == '0' && bit_test_1[36] == '1' || bit_test_1[34] == '0' && bit_test_1[35] == '1' && bit_test_1[39] == '1' && bit_test_1[36] == '0' || bit_test_1[34] == '1' && bit_test_1[35] == '0' && bit_test_1[39] == '0' && bit_test_1[36] == '1' || bit_test_1[34] == '1' && bit_test_1[35] == '0' && bit_test_1[39] == '1' && bit_test_1[36] == '0' || bit_test_1[34] == '1' && bit_test_1[35] == '1' && bit_test_1[39] == '0' && bit_test_1[36] == '0' || bit_test_1[34] == '1' && bit_test_1[35] == '1' && bit_test_1[39] == '1' && bit_test_1[36] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 41:
		{
			if (bit_test_1[37] == '1' && bit_test_1[40] == '1' || bit_test_1[37] == '0' && bit_test_1[40] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 42:
		{
			if (bit_test_1[34] == '0' && bit_test_1[37] == '0' && bit_test_1[38] == '0' && bit_test_1[41] == '0' || bit_test_1[34] == '0' && bit_test_1[37] == '0' && bit_test_1[38] == '1' && bit_test_1[41] == '1' || bit_test_1[34] == '0' && bit_test_1[37] == '1' && bit_test_1[38] == '0' && bit_test_1[41] == '1' || bit_test_1[34] == '0' && bit_test_1[37] == '1' && bit_test_1[38] == '1' && bit_test_1[41] == '0' || bit_test_1[34] == '1' && bit_test_1[37] == '0' && bit_test_1[38] == '0' && bit_test_1[41] == '1' || bit_test_1[34] == '1' && bit_test_1[37] == '0' && bit_test_1[38] == '1' && bit_test_1[41] == '0' || bit_test_1[34] == '1' && bit_test_1[37] == '1' && bit_test_1[38] == '0' && bit_test_1[41] == '0' || bit_test_1[34] == '1' && bit_test_1[37] == '1' && bit_test_1[38] == '1' && bit_test_1[41] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 43:
		{
			if (bit_test_1[36] == '0' && bit_test_1[39] == '0' && bit_test_1[38] == '0' && bit_test_1[42] == '0' || bit_test_1[36] == '0' && bit_test_1[39] == '0' && bit_test_1[38] == '1' && bit_test_1[42] == '1' || bit_test_1[36] == '0' && bit_test_1[39] == '1' && bit_test_1[38] == '0' && bit_test_1[42] == '1' || bit_test_1[36] == '0' && bit_test_1[39] == '1' && bit_test_1[38] == '1' && bit_test_1[42] == '0' || bit_test_1[36] == '1' && bit_test_1[39] == '0' && bit_test_1[38] == '0' && bit_test_1[42] == '1' || bit_test_1[36] == '1' && bit_test_1[39] == '0' && bit_test_1[38] == '1' && bit_test_1[42] == '0' || bit_test_1[36] == '1' && bit_test_1[39] == '1' && bit_test_1[38] == '0' && bit_test_1[42] == '0' || bit_test_1[36] == '1' && bit_test_1[39] == '1' && bit_test_1[38] == '1' && bit_test_1[42] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 44:
		{
			if (bit_test_1[43] == '0' && bit_test_1[37] == '0' && bit_test_1[38] == '0' && bit_test_1[41] == '0' || bit_test_1[43] == '0' && bit_test_1[37] == '0' && bit_test_1[38] == '1' && bit_test_1[41] == '1' || bit_test_1[43] == '0' && bit_test_1[37] == '1' && bit_test_1[38] == '0' && bit_test_1[41] == '1' || bit_test_1[43] == '0' && bit_test_1[37] == '1' && bit_test_1[38] == '1' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[37] == '0' && bit_test_1[38] == '0' && bit_test_1[41] == '1' || bit_test_1[43] == '1' && bit_test_1[37] == '0' && bit_test_1[38] == '1' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[37] == '1' && bit_test_1[38] == '0' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[37] == '1' && bit_test_1[38] == '1' && bit_test_1[41] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 45:
		{
			if (bit_test_1[43] == '0' && bit_test_1[40] == '0' && bit_test_1[44] == '0' && bit_test_1[41] == '0' || bit_test_1[43] == '0' && bit_test_1[40] == '0' && bit_test_1[44] == '1' && bit_test_1[41] == '1' || bit_test_1[43] == '0' && bit_test_1[40] == '1' && bit_test_1[44] == '0' && bit_test_1[41] == '1' || bit_test_1[43] == '0' && bit_test_1[40] == '1' && bit_test_1[44] == '1' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '0' && bit_test_1[44] == '0' && bit_test_1[41] == '1' || bit_test_1[43] == '1' && bit_test_1[40] == '0' && bit_test_1[44] == '1' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '1' && bit_test_1[44] == '0' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '1' && bit_test_1[44] == '1' && bit_test_1[41] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 46:
		{
			if (bit_test_1[37] == '0' && bit_test_1[39] == '0' && bit_test_1[38] == '0' && bit_test_1[45] == '0' || bit_test_1[37] == '0' && bit_test_1[39] == '0' && bit_test_1[38] == '1' && bit_test_1[45] == '1' || bit_test_1[37] == '0' && bit_test_1[39] == '1' && bit_test_1[38] == '0' && bit_test_1[45] == '1' || bit_test_1[37] == '0' && bit_test_1[39] == '1' && bit_test_1[38] == '1' && bit_test_1[45] == '0' || bit_test_1[37] == '1' && bit_test_1[39] == '0' && bit_test_1[38] == '0' && bit_test_1[45] == '1' || bit_test_1[37] == '1' && bit_test_1[39] == '0' && bit_test_1[38] == '1' && bit_test_1[45] == '0' || bit_test_1[37] == '1' && bit_test_1[39] == '1' && bit_test_1[38] == '0' && bit_test_1[45] == '0' || bit_test_1[37] == '1' && bit_test_1[39] == '1' && bit_test_1[38] == '1' && bit_test_1[45] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 47:
		{
			if (bit_test_1[41] == '1' && bit_test_1[46] == '1' || bit_test_1[41] == '0' && bit_test_1[46] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 48:
		{
			if (bit_test_1[43] == '0' && bit_test_1[40] == '0' && bit_test_1[38] == '0' && bit_test_1[47] == '0' || bit_test_1[43] == '0' && bit_test_1[40] == '0' && bit_test_1[38] == '1' && bit_test_1[47] == '1' || bit_test_1[43] == '0' && bit_test_1[40] == '1' && bit_test_1[38] == '0' && bit_test_1[47] == '1' || bit_test_1[43] == '0' && bit_test_1[40] == '1' && bit_test_1[38] == '1' && bit_test_1[47] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '0' && bit_test_1[38] == '0' && bit_test_1[47] == '1' || bit_test_1[43] == '1' && bit_test_1[40] == '0' && bit_test_1[38] == '1' && bit_test_1[47] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '1' && bit_test_1[38] == '0' && bit_test_1[47] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '1' && bit_test_1[38] == '1' && bit_test_1[47] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 49:
		{
			if (bit_test_1[39] == '1' && bit_test_1[48] == '1' || bit_test_1[39] == '0' && bit_test_1[48] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 50:
		{
			if (bit_test_1[45] == '0' && bit_test_1[46] == '0' && bit_test_1[47] == '0' && bit_test_1[49] == '0' || bit_test_1[45] == '0' && bit_test_1[46] == '0' && bit_test_1[47] == '1' && bit_test_1[49] == '1' || bit_test_1[45] == '0' && bit_test_1[46] == '1' && bit_test_1[47] == '0' && bit_test_1[49] == '1' || bit_test_1[45] == '0' && bit_test_1[46] == '1' && bit_test_1[47] == '1' && bit_test_1[49] == '0' || bit_test_1[45] == '1' && bit_test_1[46] == '0' && bit_test_1[47] == '0' && bit_test_1[49] == '1' || bit_test_1[45] == '1' && bit_test_1[46] == '0' && bit_test_1[47] == '1' && bit_test_1[49] == '0' || bit_test_1[45] == '1' && bit_test_1[46] == '1' && bit_test_1[47] == '0' && bit_test_1[49] == '0' || bit_test_1[45] == '1' && bit_test_1[46] == '1' && bit_test_1[47] == '1' && bit_test_1[49] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 51:
		{
			if (bit_test_1[44] == '0' && bit_test_1[50] == '0' && bit_test_1[47] == '0' && bit_test_1[49] == '0' || bit_test_1[44] == '0' && bit_test_1[50] == '0' && bit_test_1[47] == '1' && bit_test_1[49] == '1' || bit_test_1[44] == '0' && bit_test_1[50] == '1' && bit_test_1[47] == '0' && bit_test_1[49] == '1' || bit_test_1[44] == '0' && bit_test_1[50] == '1' && bit_test_1[47] == '1' && bit_test_1[49] == '0' || bit_test_1[44] == '1' && bit_test_1[50] == '0' && bit_test_1[47] == '0' && bit_test_1[49] == '1' || bit_test_1[44] == '1' && bit_test_1[50] == '0' && bit_test_1[47] == '1' && bit_test_1[49] == '0' || bit_test_1[44] == '1' && bit_test_1[50] == '1' && bit_test_1[47] == '0' && bit_test_1[49] == '0' || bit_test_1[44] == '1' && bit_test_1[50] == '1' && bit_test_1[47] == '1' && bit_test_1[49] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 52:
		{
			if (bit_test_1[51] == '1' && bit_test_1[48] == '1' || bit_test_1[51] == '0' && bit_test_1[48] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 53:
		{
			if (bit_test_1[50] == '0' && bit_test_1[46] == '0' && bit_test_1[51] == '0' && bit_test_1[52] == '0' || bit_test_1[50] == '0' && bit_test_1[46] == '0' && bit_test_1[51] == '1' && bit_test_1[52] == '1' || bit_test_1[50] == '0' && bit_test_1[46] == '1' && bit_test_1[51] == '0' && bit_test_1[52] == '1' || bit_test_1[50] == '0' && bit_test_1[46] == '1' && bit_test_1[51] == '1' && bit_test_1[52] == '0' || bit_test_1[50] == '1' && bit_test_1[46] == '0' && bit_test_1[51] == '0' && bit_test_1[52] == '1' || bit_test_1[50] == '1' && bit_test_1[46] == '0' && bit_test_1[51] == '1' && bit_test_1[52] == '0' || bit_test_1[50] == '1' && bit_test_1[46] == '1' && bit_test_1[51] == '0' && bit_test_1[52] == '0' || bit_test_1[50] == '1' && bit_test_1[46] == '1' && bit_test_1[51] == '1' && bit_test_1[52] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 54:
		{
			if (bit_test_1[45] == '0' && bit_test_1[50] == '0' && bit_test_1[47] == '0' && bit_test_1[53] == '0' || bit_test_1[45] == '0' && bit_test_1[50] == '0' && bit_test_1[47] == '1' && bit_test_1[53] == '1' || bit_test_1[45] == '0' && bit_test_1[50] == '1' && bit_test_1[47] == '0' && bit_test_1[53] == '1' || bit_test_1[45] == '0' && bit_test_1[50] == '1' && bit_test_1[47] == '1' && bit_test_1[53] == '0' || bit_test_1[45] == '1' && bit_test_1[50] == '0' && bit_test_1[47] == '0' && bit_test_1[53] == '1' || bit_test_1[45] == '1' && bit_test_1[50] == '0' && bit_test_1[47] == '1' && bit_test_1[53] == '0' || bit_test_1[45] == '1' && bit_test_1[50] == '1' && bit_test_1[47] == '0' && bit_test_1[53] == '0' || bit_test_1[45] == '1' && bit_test_1[50] == '1' && bit_test_1[47] == '1' && bit_test_1[53] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 55:
		{
			if (bit_test_1[54] == '1' && bit_test_1[30] == '1' || bit_test_1[54] == '0' && bit_test_1[30] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 56:
		{
			if (bit_test_1[51] == '0' && bit_test_1[48] == '0' && bit_test_1[53] == '0' && bit_test_1[55] == '0' || bit_test_1[51] == '0' && bit_test_1[48] == '0' && bit_test_1[53] == '1' && bit_test_1[55] == '1' || bit_test_1[51] == '0' && bit_test_1[48] == '1' && bit_test_1[53] == '0' && bit_test_1[55] == '1' || bit_test_1[51] == '0' && bit_test_1[48] == '1' && bit_test_1[53] == '1' && bit_test_1[55] == '0' || bit_test_1[51] == '1' && bit_test_1[48] == '0' && bit_test_1[53] == '0' && bit_test_1[55] == '1' || bit_test_1[51] == '1' && bit_test_1[48] == '0' && bit_test_1[53] == '1' && bit_test_1[55] == '0' || bit_test_1[51] == '1' && bit_test_1[48] == '1' && bit_test_1[53] == '0' && bit_test_1[55] == '0' || bit_test_1[51] == '1' && bit_test_1[48] == '1' && bit_test_1[53] == '1' && bit_test_1[55] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 57:
		{
			if (bit_test_1[56] == '1' && bit_test_1[49] == '1' || bit_test_1[56] == '0' && bit_test_1[49] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 58:
		{
			if (bit_test_1[57] == '1' && bit_test_1[38] == '1' || bit_test_1[57] == '0' && bit_test_1[38] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 59:
		{
			if (bit_test_1[54] == '0' && bit_test_1[56] == '0' && bit_test_1[51] == '0' && bit_test_1[58] == '0' || bit_test_1[54] == '0' && bit_test_1[56] == '0' && bit_test_1[51] == '1' && bit_test_1[58] == '1' || bit_test_1[54] == '0' && bit_test_1[56] == '1' && bit_test_1[51] == '0' && bit_test_1[58] == '1' || bit_test_1[54] == '0' && bit_test_1[56] == '1' && bit_test_1[51] == '1' && bit_test_1[58] == '0' || bit_test_1[54] == '1' && bit_test_1[56] == '0' && bit_test_1[51] == '0' && bit_test_1[58] == '1' || bit_test_1[54] == '1' && bit_test_1[56] == '0' && bit_test_1[51] == '1' && bit_test_1[58] == '0' || bit_test_1[54] == '1' && bit_test_1[56] == '1' && bit_test_1[51] == '0' && bit_test_1[58] == '0' || bit_test_1[54] == '1' && bit_test_1[56] == '1' && bit_test_1[51] == '1' && bit_test_1[58] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 60:
		{
			if (bit_test_1[59] == '1' && bit_test_1[58] == '1' || bit_test_1[59] == '0' && bit_test_1[58] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 61:
		{
			if (bit_test_1[55] == '0' && bit_test_1[59] == '0' && bit_test_1[60] == '0' && bit_test_1[58] == '0' || bit_test_1[55] == '0' && bit_test_1[59] == '0' && bit_test_1[60] == '1' && bit_test_1[58] == '1' || bit_test_1[55] == '0' && bit_test_1[59] == '1' && bit_test_1[60] == '0' && bit_test_1[58] == '1' || bit_test_1[55] == '0' && bit_test_1[59] == '1' && bit_test_1[60] == '1' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[59] == '0' && bit_test_1[60] == '0' && bit_test_1[58] == '1' || bit_test_1[55] == '1' && bit_test_1[59] == '0' && bit_test_1[60] == '1' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[59] == '1' && bit_test_1[60] == '0' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[59] == '1' && bit_test_1[60] == '1' && bit_test_1[58] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 62:
		{
			if (bit_test_1[55] == '0' && bit_test_1[56] == '0' && bit_test_1[60] == '0' && bit_test_1[58] == '0' || bit_test_1[55] == '0' && bit_test_1[56] == '0' && bit_test_1[60] == '1' && bit_test_1[58] == '1' || bit_test_1[55] == '0' && bit_test_1[56] == '1' && bit_test_1[60] == '0' && bit_test_1[58] == '1' || bit_test_1[55] == '0' && bit_test_1[56] == '1' && bit_test_1[60] == '1' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[56] == '0' && bit_test_1[60] == '0' && bit_test_1[58] == '1' || bit_test_1[55] == '1' && bit_test_1[56] == '0' && bit_test_1[60] == '1' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[56] == '1' && bit_test_1[60] == '0' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[56] == '1' && bit_test_1[60] == '1' && bit_test_1[58] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 63:
		{
			if (bit_test_1[61] == '1' && bit_test_1[62] == '1' || bit_test_1[61] == '0' && bit_test_1[62] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 64:
		{
			if (bit_test_1[62] == '0' && bit_test_1[59] == '0' && bit_test_1[60] == '0' && bit_test_1[63] == '0' || bit_test_1[62] == '0' && bit_test_1[59] == '0' && bit_test_1[60] == '1' && bit_test_1[63] == '1' || bit_test_1[62] == '0' && bit_test_1[59] == '1' && bit_test_1[60] == '0' && bit_test_1[63] == '1' || bit_test_1[62] == '0' && bit_test_1[59] == '1' && bit_test_1[60] == '1' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[59] == '0' && bit_test_1[60] == '0' && bit_test_1[63] == '1' || bit_test_1[62] == '1' && bit_test_1[59] == '0' && bit_test_1[60] == '1' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[59] == '1' && bit_test_1[60] == '0' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[59] == '1' && bit_test_1[60] == '1' && bit_test_1[63] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 65:
		{
			if (bit_test_1[64] == '1' && bit_test_1[46] == '1' || bit_test_1[64] == '0' && bit_test_1[46] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 66:
		{
			if (bit_test_1[56] == '0' && bit_test_1[59] == '0' && bit_test_1[65] == '0' && bit_test_1[57] == '0' || bit_test_1[56] == '0' && bit_test_1[59] == '0' && bit_test_1[65] == '1' && bit_test_1[57] == '1' || bit_test_1[56] == '0' && bit_test_1[59] == '1' && bit_test_1[65] == '0' && bit_test_1[57] == '1' || bit_test_1[56] == '0' && bit_test_1[59] == '1' && bit_test_1[65] == '1' && bit_test_1[57] == '0' || bit_test_1[56] == '1' && bit_test_1[59] == '0' && bit_test_1[65] == '0' && bit_test_1[57] == '1' || bit_test_1[56] == '1' && bit_test_1[59] == '0' && bit_test_1[65] == '1' && bit_test_1[57] == '0' || bit_test_1[56] == '1' && bit_test_1[59] == '1' && bit_test_1[65] == '0' && bit_test_1[57] == '0' || bit_test_1[56] == '1' && bit_test_1[59] == '1' && bit_test_1[65] == '1' && bit_test_1[57] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 67:
		{
			if (bit_test_1[61] == '0' && bit_test_1[64] == '0' && bit_test_1[65] == '0' && bit_test_1[66] == '0' || bit_test_1[61] == '0' && bit_test_1[64] == '0' && bit_test_1[65] == '1' && bit_test_1[66] == '1' || bit_test_1[61] == '0' && bit_test_1[64] == '1' && bit_test_1[65] == '0' && bit_test_1[66] == '1' || bit_test_1[61] == '0' && bit_test_1[64] == '1' && bit_test_1[65] == '1' && bit_test_1[66] == '0' || bit_test_1[61] == '1' && bit_test_1[64] == '0' && bit_test_1[65] == '0' && bit_test_1[66] == '1' || bit_test_1[61] == '1' && bit_test_1[64] == '0' && bit_test_1[65] == '1' && bit_test_1[66] == '0' || bit_test_1[61] == '1' && bit_test_1[64] == '1' && bit_test_1[65] == '0' && bit_test_1[66] == '0' || bit_test_1[61] == '1' && bit_test_1[64] == '1' && bit_test_1[65] == '1' && bit_test_1[66] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 68:
		{
			if (bit_test_1[67] == '1' && bit_test_1[58] == '1' || bit_test_1[67] == '0' && bit_test_1[58] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 69:
		{
			if (bit_test_1[62] == '0' && bit_test_1[66] == '0' && bit_test_1[68] == '0' && bit_test_1[63] == '0' || bit_test_1[62] == '0' && bit_test_1[66] == '0' && bit_test_1[68] == '1' && bit_test_1[63] == '1' || bit_test_1[62] == '0' && bit_test_1[66] == '1' && bit_test_1[68] == '0' && bit_test_1[63] == '1' || bit_test_1[62] == '0' && bit_test_1[66] == '1' && bit_test_1[68] == '1' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[66] == '0' && bit_test_1[68] == '0' && bit_test_1[63] == '1' || bit_test_1[62] == '1' && bit_test_1[66] == '0' && bit_test_1[68] == '1' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[66] == '1' && bit_test_1[68] == '0' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[66] == '1' && bit_test_1[68] == '1' && bit_test_1[63] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 70:
		{
			if (bit_test_1[68] == '0' && bit_test_1[64] == '0' && bit_test_1[69] == '0' && bit_test_1[66] == '0' || bit_test_1[68] == '0' && bit_test_1[64] == '0' && bit_test_1[69] == '1' && bit_test_1[66] == '1' || bit_test_1[68] == '0' && bit_test_1[64] == '1' && bit_test_1[69] == '0' && bit_test_1[66] == '1' || bit_test_1[68] == '0' && bit_test_1[64] == '1' && bit_test_1[69] == '1' && bit_test_1[66] == '0' || bit_test_1[68] == '1' && bit_test_1[64] == '0' && bit_test_1[69] == '0' && bit_test_1[66] == '1' || bit_test_1[68] == '1' && bit_test_1[64] == '0' && bit_test_1[69] == '1' && bit_test_1[66] == '0' || bit_test_1[68] == '1' && bit_test_1[64] == '1' && bit_test_1[69] == '0' && bit_test_1[66] == '0' || bit_test_1[68] == '1' && bit_test_1[64] == '1' && bit_test_1[69] == '1' && bit_test_1[66] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		//add more number
		}




		switch (size(bit_test_2))
		{

		case 2:
		{
			if (bit_test_2[0] == '1' && bit_test_2[1] == '1' || bit_test_2[0] == '0' && bit_test_2[1] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 3:
		{
			if (bit_test_2[1] == '1' && bit_test_2[2] == '1' || bit_test_2[1] == '0' && bit_test_2[2] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 4:
		{
			if (bit_test_2[3] == '1' && bit_test_2[2] == '1' || bit_test_2[3] == '0' && bit_test_2[2] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 5:
		{
			if (bit_test_2[2] == '1' && bit_test_2[4] == '1' || bit_test_2[2] == '0' && bit_test_2[4] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 6:
		{
			if (bit_test_2[5] == '1' && bit_test_2[4] == '1' || bit_test_2[5] == '0' && bit_test_2[4] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 7:
		{
			if (bit_test_2[3] == '1' && bit_test_2[6] == '1' || bit_test_2[3] == '0' && bit_test_2[6] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 8:
		{
			if (bit_test_2[3] == '0' && bit_test_2[4] == '0' && bit_test_2[5] == '0' && bit_test_2[7] == '0' || bit_test_2[3] == '0' && bit_test_2[4] == '0' && bit_test_2[5] == '1' && bit_test_2[7] == '1' || bit_test_2[3] == '0' && bit_test_2[4] == '1' && bit_test_2[5] == '0' && bit_test_2[7] == '1' || bit_test_2[3] == '0' && bit_test_2[4] == '1' && bit_test_2[5] == '1' && bit_test_2[7] == '0' || bit_test_2[3] == '1' && bit_test_2[4] == '0' && bit_test_2[5] == '0' && bit_test_2[7] == '1' || bit_test_2[3] == '1' && bit_test_2[4] == '0' && bit_test_2[5] == '1' && bit_test_2[7] == '0' || bit_test_2[3] == '1' && bit_test_2[4] == '1' && bit_test_2[5] == '0' && bit_test_2[7] == '0' || bit_test_2[3] == '1' && bit_test_2[4] == '1' && bit_test_2[5] == '1' && bit_test_2[7] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 9:
		{
			if (bit_test_2[4] == '1' && bit_test_2[8] == '1' || bit_test_2[4] == '0' && bit_test_2[8] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 10:
		{
			if (bit_test_2[6] == '1' && bit_test_2[9] == '1' || bit_test_2[6] == '0' && bit_test_2[9] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 11:
		{
			if (bit_test_2[8] == '1' && bit_test_2[10] == '1' || bit_test_2[8] == '0' && bit_test_2[10] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 12:
		{
			if (bit_test_2[5] == '0' && bit_test_2[7] == '0' && bit_test_2[10] == '0' && bit_test_2[11] == '0' || bit_test_2[5] == '0' && bit_test_2[7] == '0' && bit_test_2[10] == '1' && bit_test_2[11] == '1' || bit_test_2[5] == '0' && bit_test_2[7] == '1' && bit_test_2[10] == '0' && bit_test_2[11] == '1' || bit_test_2[5] == '0' && bit_test_2[7] == '1' && bit_test_2[10] == '1' && bit_test_2[11] == '0' || bit_test_2[5] == '1' && bit_test_2[7] == '0' && bit_test_2[10] == '0' && bit_test_2[11] == '1' || bit_test_2[5] == '1' && bit_test_2[7] == '0' && bit_test_2[10] == '1' && bit_test_2[11] == '0' || bit_test_2[5] == '1' && bit_test_2[7] == '1' && bit_test_2[10] == '0' && bit_test_2[11] == '0' || bit_test_2[5] == '1' && bit_test_2[7] == '1' && bit_test_2[10] == '1' && bit_test_2[11] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 13:
		{
			if (bit_test_2[8] == '0' && bit_test_2[9] == '0' && bit_test_2[11] == '0' && bit_test_2[12] == '0' || bit_test_2[8] == '0' && bit_test_2[9] == '0' && bit_test_2[11] == '1' && bit_test_2[12] == '1' || bit_test_2[8] == '0' && bit_test_2[9] == '1' && bit_test_2[11] == '0' && bit_test_2[12] == '1' || bit_test_2[8] == '0' && bit_test_2[9] == '1' && bit_test_2[11] == '1' && bit_test_2[12] == '0' || bit_test_2[8] == '1' && bit_test_2[9] == '0' && bit_test_2[11] == '0' && bit_test_2[12] == '1' || bit_test_2[8] == '1' && bit_test_2[9] == '0' && bit_test_2[11] == '1' && bit_test_2[12] == '0' || bit_test_2[8] == '1' && bit_test_2[9] == '1' && bit_test_2[11] == '0' && bit_test_2[12] == '0' || bit_test_2[8] == '1' && bit_test_2[9] == '1' && bit_test_2[11] == '1' && bit_test_2[12] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 14:
		{
			if (bit_test_2[3] == '0' && bit_test_2[7] == '0' && bit_test_2[12] == '0' && bit_test_2[13] == '0' || bit_test_2[3] == '0' && bit_test_2[7] == '0' && bit_test_2[12] == '1' && bit_test_2[13] == '1' || bit_test_2[3] == '0' && bit_test_2[7] == '1' && bit_test_2[12] == '0' && bit_test_2[13] == '1' || bit_test_2[3] == '0' && bit_test_2[7] == '1' && bit_test_2[12] == '1' && bit_test_2[13] == '0' || bit_test_2[3] == '1' && bit_test_2[7] == '0' && bit_test_2[12] == '0' && bit_test_2[13] == '1' || bit_test_2[3] == '1' && bit_test_2[7] == '0' && bit_test_2[12] == '1' && bit_test_2[13] == '0' || bit_test_2[3] == '1' && bit_test_2[7] == '1' && bit_test_2[12] == '0' && bit_test_2[13] == '0' || bit_test_2[3] == '1' && bit_test_2[7] == '1' && bit_test_2[12] == '1' && bit_test_2[13] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 15:
		{
			if (bit_test_2[14] == '1' && bit_test_2[13] == '1' || bit_test_2[14] == '0' && bit_test_2[13] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 16:
		{
			if (bit_test_2[3] == '0' && bit_test_2[14] == '0' && bit_test_2[12] == '0' && bit_test_2[15] == '0' || bit_test_2[3] == '0' && bit_test_2[14] == '0' && bit_test_2[12] == '1' && bit_test_2[15] == '1' || bit_test_2[3] == '0' && bit_test_2[14] == '1' && bit_test_2[12] == '0' && bit_test_2[15] == '1' || bit_test_2[3] == '0' && bit_test_2[14] == '1' && bit_test_2[12] == '1' && bit_test_2[15] == '0' || bit_test_2[3] == '1' && bit_test_2[14] == '0' && bit_test_2[12] == '0' && bit_test_2[15] == '1' || bit_test_2[3] == '1' && bit_test_2[14] == '0' && bit_test_2[12] == '1' && bit_test_2[15] == '0' || bit_test_2[3] == '1' && bit_test_2[14] == '1' && bit_test_2[12] == '0' && bit_test_2[15] == '0' || bit_test_2[3] == '1' && bit_test_2[14] == '1' && bit_test_2[12] == '1' && bit_test_2[15] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 17:
		{
			if (bit_test_2[16] == '1' && bit_test_2[13] == '1' || bit_test_2[16] == '0' && bit_test_2[13] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 18:
		{
			if (bit_test_2[10] == '1' && bit_test_2[17] == '1' || bit_test_2[10] == '0' && bit_test_2[17] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 19:
		{
			if (bit_test_2[13] == '0' && bit_test_2[16] == '0' && bit_test_2[17] == '0' && bit_test_2[18] == '0' || bit_test_2[13] == '0' && bit_test_2[16] == '0' && bit_test_2[17] == '1' && bit_test_2[18] == '1' || bit_test_2[13] == '0' && bit_test_2[16] == '1' && bit_test_2[17] == '0' && bit_test_2[18] == '1' || bit_test_2[13] == '0' && bit_test_2[16] == '1' && bit_test_2[17] == '1' && bit_test_2[18] == '0' || bit_test_2[13] == '1' && bit_test_2[16] == '0' && bit_test_2[17] == '0' && bit_test_2[18] == '1' || bit_test_2[13] == '1' && bit_test_2[16] == '0' && bit_test_2[17] == '1' && bit_test_2[18] == '0' || bit_test_2[13] == '1' && bit_test_2[16] == '1' && bit_test_2[17] == '0' && bit_test_2[18] == '0' || bit_test_2[13] == '1' && bit_test_2[16] == '1' && bit_test_2[17] == '1' && bit_test_2[18] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 20:
		{
			if (bit_test_2[16] == '1' && bit_test_2[19] == '1' || bit_test_2[16] == '0' && bit_test_2[19] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 21:
		{
			if (bit_test_2[18] == '1' && bit_test_2[20] == '1' || bit_test_2[18] == '0' && bit_test_2[20] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 22:
		{
			if (bit_test_2[21] == '1' && bit_test_2[20] == '1' || bit_test_2[21] == '0' && bit_test_2[20] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 23:
		{
			if (bit_test_2[17] == '1' && bit_test_2[22] == '1' || bit_test_2[17] == '0' && bit_test_2[22] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 24:
		{
			if (bit_test_2[21] == '0' && bit_test_2[16] == '0' && bit_test_2[22] == '0' && bit_test_2[23] == '0' || bit_test_2[21] == '0' && bit_test_2[16] == '0' && bit_test_2[22] == '1' && bit_test_2[23] == '1' || bit_test_2[21] == '0' && bit_test_2[16] == '1' && bit_test_2[22] == '0' && bit_test_2[23] == '1' || bit_test_2[21] == '0' && bit_test_2[16] == '1' && bit_test_2[22] == '1' && bit_test_2[23] == '0' || bit_test_2[16] == '1' && bit_test_2[21] == '0' && bit_test_2[22] == '0' && bit_test_2[23] == '1' || bit_test_2[21] == '1' && bit_test_2[16] == '0' && bit_test_2[22] == '1' && bit_test_2[23] == '0' || bit_test_2[21] == '1' && bit_test_2[16] == '1' && bit_test_2[22] == '0' && bit_test_2[23] == '0' || bit_test_2[21] == '1' && bit_test_2[16] == '1' && bit_test_2[22] == '1' && bit_test_2[23] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 25:
		{
			if (bit_test_2[21] == '1' && bit_test_2[24] == '1' || bit_test_2[21] == '0' && bit_test_2[24] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 26:
		{
			if (bit_test_2[19] == '0' && bit_test_2[24] == '0' && bit_test_2[25] == '0' && bit_test_2[23] == '0' || bit_test_2[24] == '0' && bit_test_2[19] == '0' && bit_test_2[25] == '1' && bit_test_2[23] == '1' || bit_test_2[24] == '0' && bit_test_2[19] == '1' && bit_test_2[25] == '0' && bit_test_2[23] == '1' || bit_test_2[24] == '0' && bit_test_2[19] == '1' && bit_test_2[25] == '1' && bit_test_2[23] == '0' || bit_test_2[19] == '1' && bit_test_2[24] == '0' && bit_test_2[25] == '0' && bit_test_2[23] == '1' || bit_test_2[24] == '1' && bit_test_2[19] == '0' && bit_test_2[25] == '1' && bit_test_2[23] == '0' || bit_test_2[24] == '1' && bit_test_2[19] == '1' && bit_test_2[25] == '0' && bit_test_2[23] == '0' || bit_test_2[24] == '1' && bit_test_2[19] == '1' && bit_test_2[25] == '1' && bit_test_2[23] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 27:
		{
			if (bit_test_2[21] == '0' && bit_test_2[24] == '0' && bit_test_2[25] == '0' && bit_test_2[26] == '0' || bit_test_2[24] == '0' && bit_test_2[21] == '0' && bit_test_2[25] == '1' && bit_test_2[26] == '1' || bit_test_2[24] == '0' && bit_test_2[21] == '1' && bit_test_2[25] == '0' && bit_test_2[26] == '1' || bit_test_2[24] == '0' && bit_test_2[21] == '1' && bit_test_2[25] == '1' && bit_test_2[26] == '0' || bit_test_2[21] == '1' && bit_test_2[24] == '0' && bit_test_2[25] == '0' && bit_test_2[26] == '1' || bit_test_2[24] == '1' && bit_test_2[21] == '0' && bit_test_2[25] == '1' && bit_test_2[26] == '0' || bit_test_2[24] == '1' && bit_test_2[21] == '1' && bit_test_2[25] == '0' && bit_test_2[26] == '0' || bit_test_2[24] == '1' && bit_test_2[21] == '1' && bit_test_2[25] == '1' && bit_test_2[26] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 28:
		{
			if (bit_test_2[27] == '1' && bit_test_2[24] == '1' || bit_test_2[27] == '0' && bit_test_2[24] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 29:
		{
			if (bit_test_2[26] == '1' && bit_test_2[28] == '1' || bit_test_2[26] == '0' && bit_test_2[28] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 30:
		{
			if (bit_test_2[6] == '0' && bit_test_2[27] == '0' && bit_test_2[28] == '0' && bit_test_2[29] == '0' || bit_test_2[6] == '0' && bit_test_2[27] == '0' && bit_test_2[28] == '1' && bit_test_2[29] == '1' || bit_test_2[6] == '0' && bit_test_2[27] == '1' && bit_test_2[28] == '0' && bit_test_2[29] == '1' || bit_test_2[6] == '0' && bit_test_2[27] == '1' && bit_test_2[28] == '1' && bit_test_2[29] == '0' || bit_test_2[6] == '1' && bit_test_2[27] == '0' && bit_test_2[28] == '0' && bit_test_2[29] == '1' || bit_test_2[6] == '1' && bit_test_2[27] == '0' && bit_test_2[28] == '1' && bit_test_2[29] == '0' || bit_test_2[6] == '1' && bit_test_2[27] == '1' && bit_test_2[28] == '0' && bit_test_2[29] == '0' || bit_test_2[6] == '1' && bit_test_2[27] == '1' && bit_test_2[28] == '1' && bit_test_2[29] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 31:
		{
			if (bit_test_2[27] == '1' && bit_test_2[30] == '1' || bit_test_2[27] == '0' && bit_test_2[30] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 32:
		{
			if (bit_test_2[9] == '0' && bit_test_2[30] == '0' && bit_test_2[31] == '0' && bit_test_2[29] == '0' || bit_test_2[9] == '0' && bit_test_2[30] == '0' && bit_test_2[31] == '1' && bit_test_2[29] == '1' || bit_test_2[9] == '0' && bit_test_2[31] == '1' && bit_test_2[30] == '0' && bit_test_2[29] == '1' || bit_test_2[9] == '0' && bit_test_2[30] == '1' && bit_test_2[31] == '1' && bit_test_2[29] == '0' || bit_test_2[9] == '1' && bit_test_2[30] == '0' && bit_test_2[31] == '0' && bit_test_2[29] == '1' || bit_test_2[9] == '1' && bit_test_2[30] == '0' && bit_test_2[31] == '1' && bit_test_2[29] == '0' || bit_test_2[9] == '1' && bit_test_2[30] == '1' && bit_test_2[31] == '0' && bit_test_2[29] == '0' || bit_test_2[9] == '1' && bit_test_2[30] == '1' && bit_test_2[31] == '1' && bit_test_2[29] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 33:
		{
			if (bit_test_2[19] == '1' && bit_test_2[32] == '1' || bit_test_2[19] == '0' && bit_test_2[32] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 34:
		{
			if (bit_test_2[29] == '0' && bit_test_2[30] == '0' && bit_test_2[33] == '0' && bit_test_2[25] == '0' || bit_test_2[25] == '0' && bit_test_2[30] == '0' && bit_test_2[33] == '1' && bit_test_2[29] == '1' || bit_test_2[25] == '0' && bit_test_2[29] == '1' && bit_test_2[30] == '0' && bit_test_2[33] == '1' || bit_test_2[25] == '0' && bit_test_2[30] == '1' && bit_test_2[33] == '1' && bit_test_2[29] == '0' || bit_test_2[25] == '1' && bit_test_2[30] == '0' && bit_test_2[33] == '0' && bit_test_2[29] == '1' || bit_test_2[25] == '1' && bit_test_2[30] == '0' && bit_test_2[33] == '1' && bit_test_2[29] == '0' || bit_test_2[25] == '1' && bit_test_2[30] == '1' && bit_test_2[33] == '0' && bit_test_2[29] == '0' || bit_test_2[25] == '1' && bit_test_2[30] == '1' && bit_test_2[33] == '1' && bit_test_2[29] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 35:
		{
			if (bit_test_2[34] == '1' && bit_test_2[32] == '1' || bit_test_2[34] == '0' && bit_test_2[32] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 36:
		{
			if (bit_test_2[24] == '1' && bit_test_2[35] == '1' || bit_test_2[24] == '0' && bit_test_2[35] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 37:
		{
			if (bit_test_2[32] == '0' && bit_test_2[30] == '0' && bit_test_2[36] == '0' && bit_test_2[35] == '0' || bit_test_2[30] == '0' && bit_test_2[32] == '0' && bit_test_2[35] == '1' && bit_test_2[36] == '1' || bit_test_2[30] == '0' && bit_test_2[32] == '1' && bit_test_2[35] == '0' && bit_test_2[36] == '1' || bit_test_2[30] == '0' && bit_test_2[32] == '1' && bit_test_2[35] == '1' && bit_test_2[36] == '0' || bit_test_2[30] == '1' && bit_test_2[32] == '0' && bit_test_2[35] == '0' && bit_test_2[36] == '1' || bit_test_2[30] == '1' && bit_test_2[32] == '0' && bit_test_2[35] == '1' && bit_test_2[36] == '0' || bit_test_2[30] == '1' && bit_test_2[32] == '1' && bit_test_2[35] == '0' && bit_test_2[36] == '0' || bit_test_2[30] == '1' && bit_test_2[32] == '1' && bit_test_2[35] == '1' && bit_test_2[36] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 38:
		{
			if (bit_test_2[32] == '0' && bit_test_2[31] == '0' && bit_test_2[36] == '0' && bit_test_2[37] == '0' || bit_test_2[31] == '0' && bit_test_2[32] == '0' && bit_test_2[37] == '1' && bit_test_2[36] == '1' || bit_test_2[31] == '0' && bit_test_2[32] == '1' && bit_test_2[37] == '0' && bit_test_2[36] == '1' || bit_test_2[31] == '0' && bit_test_2[32] == '1' && bit_test_2[37] == '1' && bit_test_2[36] == '0' || bit_test_2[31] == '1' && bit_test_2[32] == '0' && bit_test_2[37] == '0' && bit_test_2[36] == '1' || bit_test_2[31] == '1' && bit_test_2[32] == '0' && bit_test_2[37] == '1' && bit_test_2[36] == '0' || bit_test_2[31] == '1' && bit_test_2[32] == '1' && bit_test_2[37] == '0' && bit_test_2[36] == '0' || bit_test_2[31] == '1' && bit_test_2[32] == '1' && bit_test_2[37] == '1' && bit_test_2[36] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 39:
		{
			if (bit_test_2[34] == '1' && bit_test_2[38] == '1' || bit_test_2[34] == '0' && bit_test_2[38] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 40:
		{
			if (bit_test_2[34] == '0' && bit_test_2[35] == '0' && bit_test_2[36] == '0' && bit_test_2[39] == '0' || bit_test_2[34] == '0' && bit_test_2[35] == '0' && bit_test_2[39] == '1' && bit_test_2[36] == '1' || bit_test_2[34] == '0' && bit_test_2[35] == '1' && bit_test_2[39] == '0' && bit_test_2[36] == '1' || bit_test_2[34] == '0' && bit_test_2[35] == '1' && bit_test_2[39] == '1' && bit_test_2[36] == '0' || bit_test_2[34] == '1' && bit_test_2[35] == '0' && bit_test_2[39] == '0' && bit_test_2[36] == '1' || bit_test_2[34] == '1' && bit_test_2[35] == '0' && bit_test_2[39] == '1' && bit_test_2[36] == '0' || bit_test_2[34] == '1' && bit_test_2[35] == '1' && bit_test_2[39] == '0' && bit_test_2[36] == '0' || bit_test_2[34] == '1' && bit_test_2[35] == '1' && bit_test_2[39] == '1' && bit_test_2[36] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 41:
		{
			if (bit_test_2[37] == '1' && bit_test_2[40] == '1' || bit_test_2[37] == '0' && bit_test_2[40] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 42:
		{
			if (bit_test_2[34] == '0' && bit_test_2[37] == '0' && bit_test_2[38] == '0' && bit_test_2[41] == '0' || bit_test_2[34] == '0' && bit_test_2[37] == '0' && bit_test_2[38] == '1' && bit_test_2[41] == '1' || bit_test_2[34] == '0' && bit_test_2[37] == '1' && bit_test_2[38] == '0' && bit_test_2[41] == '1' || bit_test_2[34] == '0' && bit_test_2[37] == '1' && bit_test_2[38] == '1' && bit_test_2[41] == '0' || bit_test_2[34] == '1' && bit_test_2[37] == '0' && bit_test_2[38] == '0' && bit_test_2[41] == '1' || bit_test_2[34] == '1' && bit_test_2[37] == '0' && bit_test_2[38] == '1' && bit_test_2[41] == '0' || bit_test_2[34] == '1' && bit_test_2[37] == '1' && bit_test_2[38] == '0' && bit_test_2[41] == '0' || bit_test_2[34] == '1' && bit_test_2[37] == '1' && bit_test_2[38] == '1' && bit_test_2[41] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 43:
		{
			if (bit_test_2[36] == '0' && bit_test_2[39] == '0' && bit_test_2[38] == '0' && bit_test_2[42] == '0' || bit_test_2[36] == '0' && bit_test_2[39] == '0' && bit_test_2[38] == '1' && bit_test_2[42] == '1' || bit_test_2[36] == '0' && bit_test_2[39] == '1' && bit_test_2[38] == '0' && bit_test_2[42] == '1' || bit_test_2[36] == '0' && bit_test_2[39] == '1' && bit_test_2[38] == '1' && bit_test_2[42] == '0' || bit_test_2[36] == '1' && bit_test_2[39] == '0' && bit_test_2[38] == '0' && bit_test_2[42] == '1' || bit_test_2[36] == '1' && bit_test_2[39] == '0' && bit_test_2[38] == '1' && bit_test_2[42] == '0' || bit_test_2[36] == '1' && bit_test_2[39] == '1' && bit_test_2[38] == '0' && bit_test_2[42] == '0' || bit_test_2[36] == '1' && bit_test_2[39] == '1' && bit_test_2[38] == '1' && bit_test_2[42] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 44:
		{
			if (bit_test_2[43] == '0' && bit_test_2[37] == '0' && bit_test_2[38] == '0' && bit_test_2[41] == '0' || bit_test_2[43] == '0' && bit_test_2[37] == '0' && bit_test_2[38] == '1' && bit_test_2[41] == '1' || bit_test_2[43] == '0' && bit_test_2[37] == '1' && bit_test_2[38] == '0' && bit_test_2[41] == '1' || bit_test_2[43] == '0' && bit_test_2[37] == '1' && bit_test_2[38] == '1' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[37] == '0' && bit_test_2[38] == '0' && bit_test_2[41] == '1' || bit_test_2[43] == '1' && bit_test_2[37] == '0' && bit_test_2[38] == '1' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[37] == '1' && bit_test_2[38] == '0' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[37] == '1' && bit_test_2[38] == '1' && bit_test_2[41] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 45:
		{
			if (bit_test_2[43] == '0' && bit_test_2[40] == '0' && bit_test_2[44] == '0' && bit_test_2[41] == '0' || bit_test_2[43] == '0' && bit_test_2[40] == '0' && bit_test_2[44] == '1' && bit_test_2[41] == '1' || bit_test_2[43] == '0' && bit_test_2[40] == '1' && bit_test_2[44] == '0' && bit_test_2[41] == '1' || bit_test_2[43] == '0' && bit_test_2[40] == '1' && bit_test_2[44] == '1' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '0' && bit_test_2[44] == '0' && bit_test_2[41] == '1' || bit_test_2[43] == '1' && bit_test_2[40] == '0' && bit_test_2[44] == '1' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '1' && bit_test_2[44] == '0' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '1' && bit_test_2[44] == '1' && bit_test_2[41] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 46:
		{
			if (bit_test_2[37] == '0' && bit_test_2[39] == '0' && bit_test_2[38] == '0' && bit_test_2[45] == '0' || bit_test_2[37] == '0' && bit_test_2[39] == '0' && bit_test_2[38] == '1' && bit_test_2[45] == '1' || bit_test_2[37] == '0' && bit_test_2[39] == '1' && bit_test_2[38] == '0' && bit_test_2[45] == '1' || bit_test_2[37] == '0' && bit_test_2[39] == '1' && bit_test_2[38] == '1' && bit_test_2[45] == '0' || bit_test_2[37] == '1' && bit_test_2[39] == '0' && bit_test_2[38] == '0' && bit_test_2[45] == '1' || bit_test_2[37] == '1' && bit_test_2[39] == '0' && bit_test_2[38] == '1' && bit_test_2[45] == '0' || bit_test_2[37] == '1' && bit_test_2[39] == '1' && bit_test_2[38] == '0' && bit_test_2[45] == '0' || bit_test_2[37] == '1' && bit_test_2[39] == '1' && bit_test_2[38] == '1' && bit_test_2[45] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 47:
		{
			if (bit_test_2[41] == '1' && bit_test_2[46] == '1' || bit_test_2[41] == '0' && bit_test_2[46] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 48:
		{
			if (bit_test_2[43] == '0' && bit_test_2[40] == '0' && bit_test_2[38] == '0' && bit_test_2[47] == '0' || bit_test_2[43] == '0' && bit_test_2[40] == '0' && bit_test_2[38] == '1' && bit_test_2[47] == '1' || bit_test_2[43] == '0' && bit_test_2[40] == '1' && bit_test_2[38] == '0' && bit_test_2[47] == '1' || bit_test_2[43] == '0' && bit_test_2[40] == '1' && bit_test_2[38] == '1' && bit_test_2[47] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '0' && bit_test_2[38] == '0' && bit_test_2[47] == '1' || bit_test_2[43] == '1' && bit_test_2[40] == '0' && bit_test_2[38] == '1' && bit_test_2[47] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '1' && bit_test_2[38] == '0' && bit_test_2[47] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '1' && bit_test_2[38] == '1' && bit_test_2[47] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 49:
		{
			if (bit_test_2[39] == '1' && bit_test_2[48] == '1' || bit_test_2[39] == '0' && bit_test_2[48] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 50:
		{
			if (bit_test_2[45] == '0' && bit_test_2[46] == '0' && bit_test_2[47] == '0' && bit_test_2[49] == '0' || bit_test_2[45] == '0' && bit_test_2[46] == '0' && bit_test_2[47] == '1' && bit_test_2[49] == '1' || bit_test_2[45] == '0' && bit_test_2[46] == '1' && bit_test_2[47] == '0' && bit_test_2[49] == '1' || bit_test_2[45] == '0' && bit_test_2[46] == '1' && bit_test_2[47] == '1' && bit_test_2[49] == '0' || bit_test_2[45] == '1' && bit_test_2[46] == '0' && bit_test_2[47] == '0' && bit_test_2[49] == '1' || bit_test_2[45] == '1' && bit_test_2[46] == '0' && bit_test_2[47] == '1' && bit_test_2[49] == '0' || bit_test_2[45] == '1' && bit_test_2[46] == '1' && bit_test_2[47] == '0' && bit_test_2[49] == '0' || bit_test_2[45] == '1' && bit_test_2[46] == '1' && bit_test_2[47] == '1' && bit_test_2[49] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 51:
		{
			if (bit_test_2[44] == '0' && bit_test_2[50] == '0' && bit_test_2[47] == '0' && bit_test_2[49] == '0' || bit_test_2[44] == '0' && bit_test_2[50] == '0' && bit_test_2[47] == '1' && bit_test_2[49] == '1' || bit_test_2[44] == '0' && bit_test_2[50] == '1' && bit_test_2[47] == '0' && bit_test_2[49] == '1' || bit_test_2[44] == '0' && bit_test_2[50] == '1' && bit_test_2[47] == '1' && bit_test_2[49] == '0' || bit_test_2[44] == '1' && bit_test_2[50] == '0' && bit_test_2[47] == '0' && bit_test_2[49] == '1' || bit_test_2[44] == '1' && bit_test_2[50] == '0' && bit_test_2[47] == '1' && bit_test_2[49] == '0' || bit_test_2[44] == '1' && bit_test_2[50] == '1' && bit_test_2[47] == '0' && bit_test_2[49] == '0' || bit_test_2[44] == '1' && bit_test_2[50] == '1' && bit_test_2[47] == '1' && bit_test_2[49] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 52:
		{
			if (bit_test_2[51] == '1' && bit_test_2[48] == '1' || bit_test_2[51] == '0' && bit_test_2[48] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 53:
		{
			if (bit_test_2[50] == '0' && bit_test_2[46] == '0' && bit_test_2[51] == '0' && bit_test_2[52] == '0' || bit_test_2[50] == '0' && bit_test_2[46] == '0' && bit_test_2[51] == '1' && bit_test_2[52] == '1' || bit_test_2[50] == '0' && bit_test_2[46] == '1' && bit_test_2[51] == '0' && bit_test_2[52] == '1' || bit_test_2[50] == '0' && bit_test_2[46] == '1' && bit_test_2[51] == '1' && bit_test_2[52] == '0' || bit_test_2[50] == '1' && bit_test_2[46] == '0' && bit_test_2[51] == '0' && bit_test_2[52] == '1' || bit_test_2[50] == '1' && bit_test_2[46] == '0' && bit_test_2[51] == '1' && bit_test_2[52] == '0' || bit_test_2[50] == '1' && bit_test_2[46] == '1' && bit_test_2[51] == '0' && bit_test_2[52] == '0' || bit_test_2[50] == '1' && bit_test_2[46] == '1' && bit_test_2[51] == '1' && bit_test_2[52] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 54:
		{
			if (bit_test_2[45] == '0' && bit_test_2[50] == '0' && bit_test_2[47] == '0' && bit_test_2[53] == '0' || bit_test_2[45] == '0' && bit_test_2[50] == '0' && bit_test_2[47] == '1' && bit_test_2[53] == '1' || bit_test_2[45] == '0' && bit_test_2[50] == '1' && bit_test_2[47] == '0' && bit_test_2[53] == '1' || bit_test_2[45] == '0' && bit_test_2[50] == '1' && bit_test_2[47] == '1' && bit_test_2[53] == '0' || bit_test_2[45] == '1' && bit_test_2[50] == '0' && bit_test_2[47] == '0' && bit_test_2[53] == '1' || bit_test_2[45] == '1' && bit_test_2[50] == '0' && bit_test_2[47] == '1' && bit_test_2[53] == '0' || bit_test_2[45] == '1' && bit_test_2[50] == '1' && bit_test_2[47] == '0' && bit_test_2[53] == '0' || bit_test_2[45] == '1' && bit_test_2[50] == '1' && bit_test_2[47] == '1' && bit_test_2[53] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 55:
		{
			if (bit_test_2[54] == '1' && bit_test_2[30] == '1' || bit_test_2[54] == '0' && bit_test_2[30] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 56:
		{
			if (bit_test_2[51] == '0' && bit_test_2[48] == '0' && bit_test_2[53] == '0' && bit_test_2[55] == '0' || bit_test_2[51] == '0' && bit_test_2[48] == '0' && bit_test_2[53] == '1' && bit_test_2[55] == '1' || bit_test_2[51] == '0' && bit_test_2[48] == '1' && bit_test_2[53] == '0' && bit_test_2[55] == '1' || bit_test_2[51] == '0' && bit_test_2[48] == '1' && bit_test_2[53] == '1' && bit_test_2[55] == '0' || bit_test_2[51] == '1' && bit_test_2[48] == '0' && bit_test_2[53] == '0' && bit_test_2[55] == '1' || bit_test_2[51] == '1' && bit_test_2[48] == '0' && bit_test_2[53] == '1' && bit_test_2[55] == '0' || bit_test_2[51] == '1' && bit_test_2[48] == '1' && bit_test_2[53] == '0' && bit_test_2[55] == '0' || bit_test_2[51] == '1' && bit_test_2[48] == '1' && bit_test_2[53] == '1' && bit_test_2[55] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 57:
		{
			if (bit_test_2[56] == '1' && bit_test_2[49] == '1' || bit_test_2[56] == '0' && bit_test_2[49] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 58:
		{
			if (bit_test_2[57] == '1' && bit_test_2[38] == '1' || bit_test_2[57] == '0' && bit_test_2[38] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 59:
		{
			if (bit_test_2[54] == '0' && bit_test_2[56] == '0' && bit_test_2[51] == '0' && bit_test_2[58] == '0' || bit_test_2[54] == '0' && bit_test_2[56] == '0' && bit_test_2[51] == '1' && bit_test_2[58] == '1' || bit_test_2[54] == '0' && bit_test_2[56] == '1' && bit_test_2[51] == '0' && bit_test_2[58] == '1' || bit_test_2[54] == '0' && bit_test_2[56] == '1' && bit_test_2[51] == '1' && bit_test_2[58] == '0' || bit_test_2[54] == '1' && bit_test_2[56] == '0' && bit_test_2[51] == '0' && bit_test_2[58] == '1' || bit_test_2[54] == '1' && bit_test_2[56] == '0' && bit_test_2[51] == '1' && bit_test_2[58] == '0' || bit_test_2[54] == '1' && bit_test_2[56] == '1' && bit_test_2[51] == '0' && bit_test_2[58] == '0' || bit_test_2[54] == '1' && bit_test_2[56] == '1' && bit_test_2[51] == '1' && bit_test_2[58] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 60:
		{
			if (bit_test_2[59] == '1' && bit_test_2[58] == '1' || bit_test_2[59] == '0' && bit_test_2[58] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 61:
		{
			if (bit_test_2[55] == '0' && bit_test_2[59] == '0' && bit_test_2[60] == '0' && bit_test_2[58] == '0' || bit_test_2[55] == '0' && bit_test_2[59] == '0' && bit_test_2[60] == '1' && bit_test_2[58] == '1' || bit_test_2[55] == '0' && bit_test_2[59] == '1' && bit_test_2[60] == '0' && bit_test_2[58] == '1' || bit_test_2[55] == '0' && bit_test_2[59] == '1' && bit_test_2[60] == '1' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[59] == '0' && bit_test_2[60] == '0' && bit_test_2[58] == '1' || bit_test_2[55] == '1' && bit_test_2[59] == '0' && bit_test_2[60] == '1' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[59] == '1' && bit_test_2[60] == '0' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[59] == '1' && bit_test_2[60] == '1' && bit_test_2[58] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 62:
		{
			if (bit_test_2[55] == '0' && bit_test_2[56] == '0' && bit_test_2[60] == '0' && bit_test_2[58] == '0' || bit_test_2[55] == '0' && bit_test_2[56] == '0' && bit_test_2[60] == '1' && bit_test_2[58] == '1' || bit_test_2[55] == '0' && bit_test_2[56] == '1' && bit_test_2[60] == '0' && bit_test_2[58] == '1' || bit_test_2[55] == '0' && bit_test_2[56] == '1' && bit_test_2[60] == '1' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[56] == '0' && bit_test_2[60] == '0' && bit_test_2[58] == '1' || bit_test_2[55] == '1' && bit_test_2[56] == '0' && bit_test_2[60] == '1' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[56] == '1' && bit_test_2[60] == '0' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[56] == '1' && bit_test_2[60] == '1' && bit_test_2[58] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 63:
		{
			if (bit_test_2[61] == '1' && bit_test_2[62] == '1' || bit_test_2[61] == '0' && bit_test_2[62] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 64:
		{
			if (bit_test_2[62] == '0' && bit_test_2[59] == '0' && bit_test_2[60] == '0' && bit_test_2[63] == '0' || bit_test_2[62] == '0' && bit_test_2[59] == '0' && bit_test_2[60] == '1' && bit_test_2[63] == '1' || bit_test_2[62] == '0' && bit_test_2[59] == '1' && bit_test_2[60] == '0' && bit_test_2[63] == '1' || bit_test_2[62] == '0' && bit_test_2[59] == '1' && bit_test_2[60] == '1' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[59] == '0' && bit_test_2[60] == '0' && bit_test_2[63] == '1' || bit_test_2[62] == '1' && bit_test_2[59] == '0' && bit_test_2[60] == '1' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[59] == '1' && bit_test_2[60] == '0' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[59] == '1' && bit_test_2[60] == '1' && bit_test_2[63] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 65:
		{
			if (bit_test_2[64] == '1' && bit_test_2[46] == '1' || bit_test_2[64] == '0' && bit_test_2[46] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 66:
		{
			if (bit_test_2[56] == '0' && bit_test_2[59] == '0' && bit_test_2[65] == '0' && bit_test_2[57] == '0' || bit_test_2[56] == '0' && bit_test_2[59] == '0' && bit_test_2[65] == '1' && bit_test_2[57] == '1' || bit_test_2[56] == '0' && bit_test_2[59] == '1' && bit_test_2[65] == '0' && bit_test_2[57] == '1' || bit_test_2[56] == '0' && bit_test_2[59] == '1' && bit_test_2[65] == '1' && bit_test_2[57] == '0' || bit_test_2[56] == '1' && bit_test_2[59] == '0' && bit_test_2[65] == '0' && bit_test_2[57] == '1' || bit_test_2[56] == '1' && bit_test_2[59] == '0' && bit_test_2[65] == '1' && bit_test_2[57] == '0' || bit_test_2[56] == '1' && bit_test_2[59] == '1' && bit_test_2[65] == '0' && bit_test_2[57] == '0' || bit_test_2[56] == '1' && bit_test_2[59] == '1' && bit_test_2[65] == '1' && bit_test_2[57] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 67:
		{
			if (bit_test_2[61] == '0' && bit_test_2[64] == '0' && bit_test_2[65] == '0' && bit_test_2[66] == '0' || bit_test_2[61] == '0' && bit_test_2[64] == '0' && bit_test_2[65] == '1' && bit_test_2[66] == '1' || bit_test_2[61] == '0' && bit_test_2[64] == '1' && bit_test_2[65] == '0' && bit_test_2[66] == '1' || bit_test_2[61] == '0' && bit_test_2[64] == '1' && bit_test_2[65] == '1' && bit_test_2[66] == '0' || bit_test_2[61] == '1' && bit_test_2[64] == '0' && bit_test_2[65] == '0' && bit_test_2[66] == '1' || bit_test_2[61] == '1' && bit_test_2[64] == '0' && bit_test_2[65] == '1' && bit_test_2[66] == '0' || bit_test_2[61] == '1' && bit_test_2[64] == '1' && bit_test_2[65] == '0' && bit_test_2[66] == '0' || bit_test_2[61] == '1' && bit_test_2[64] == '1' && bit_test_2[65] == '1' && bit_test_2[66] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 68:
		{
			if (bit_test_2[67] == '1' && bit_test_2[58] == '1' || bit_test_2[67] == '0' && bit_test_2[58] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 69:
		{
			if (bit_test_2[62] == '0' && bit_test_2[66] == '0' && bit_test_2[68] == '0' && bit_test_2[63] == '0' || bit_test_2[62] == '0' && bit_test_2[66] == '0' && bit_test_2[68] == '1' && bit_test_2[63] == '1' || bit_test_2[62] == '0' && bit_test_2[66] == '1' && bit_test_2[68] == '0' && bit_test_2[63] == '1' || bit_test_2[62] == '0' && bit_test_2[66] == '1' && bit_test_2[68] == '1' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[66] == '0' && bit_test_2[68] == '0' && bit_test_2[63] == '1' || bit_test_2[62] == '1' && bit_test_2[66] == '0' && bit_test_2[68] == '1' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[66] == '1' && bit_test_2[68] == '0' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[66] == '1' && bit_test_2[68] == '1' && bit_test_2[63] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 70:
		{
			if (bit_test_2[68] == '0' && bit_test_2[64] == '0' && bit_test_2[69] == '0' && bit_test_2[66] == '0' || bit_test_2[68] == '0' && bit_test_2[64] == '0' && bit_test_2[69] == '1' && bit_test_2[66] == '1' || bit_test_2[68] == '0' && bit_test_2[64] == '1' && bit_test_2[69] == '0' && bit_test_2[66] == '1' || bit_test_2[68] == '0' && bit_test_2[64] == '1' && bit_test_2[69] == '1' && bit_test_2[66] == '0' || bit_test_2[68] == '1' && bit_test_2[64] == '0' && bit_test_2[69] == '0' && bit_test_2[66] == '1' || bit_test_2[68] == '1' && bit_test_2[64] == '0' && bit_test_2[69] == '1' && bit_test_2[66] == '0' || bit_test_2[68] == '1' && bit_test_2[64] == '1' && bit_test_2[69] == '0' && bit_test_2[66] == '0' || bit_test_2[68] == '1' && bit_test_2[64] == '1' && bit_test_2[69] == '1' && bit_test_2[66] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		//add more number
		}




		register int name_test;
		name_test = bit_test_1[0] ^ bit_test_2[0];
		ss << name_test;
		ss >> beatstream_bit;

		beatstream += beatstream_bit;
		

		if (size(beatstream) == size(bit_test_3)) {
			cout << "\nБитовый поток для шифрования:       " << beatstream << "\n";
		}

	}

	//шифрование слова
	register char element;
	register string encrypted_word = "";
	register int counter1 = 0;
	
	for (register int counter_i = 0; counter_i < size(bit_test_3); counter_i++) {
		
		if (beatstream[counter_i] == '1' && bit_test_3[counter_i] == '1' || beatstream[counter_i] == '0' && bit_test_3[counter_i] == '0') {
			encrypted_word += "0";
		}
		else {
			encrypted_word += "1";
		}

	}

	cout << "\nБитовый поток зашифрованного слова: " << encrypted_word << "\n";
	
	register int beging = 0;
	register string c = "";
	register int decimal_Number_int;
	register string a = "";

	register string bit_number = "";
	register string encrypted_total_str = "";

	//bit_3_encrypt_1(x2);
	for (register int counter_str = 0; counter_str <= size(bit_test_3); counter_str++) {
		
		register string a = encrypted_word.substr(0 + counter_str * 7, 7);
		c += a;
		decimal_Number_int = decimal_Number(a, counter_str);

		bit_number = (char)(decimal_Number_int);

		encrypted_total_str += bit_number;

		cout << "\nЭлементы бинарного потока:  " << a << "   Десятичное число элемента: " << decimal_Number_int  << "   Элемент зашифрованного слова: " << bit_number << "\n";

		if ((size(bit_test_3) - size(c)) < 7) {

			cout << "\nГотово.   Зашифрованное слово: " << encrypted_total_str << "\n";
			goto end;
		}
	}

end:
	
	return "1";
}




string decrypt_1() {
	register string number1;
	register string number2;
	register string word;

	register string test_1;
	stringstream test;

	register string test_2;
	stringstream test1;

	register string test_3;
	stringstream test2;

tryAgain_1:
	cout << "\n                   Введите первый ключ: \n";
	getline(cin, number1, ';');
	if (size(number1) > 10 || size(number1) < 2) {
		cout << "\nВведите другой набор ключей: \n";
		goto tryAgain_1;
	}

tryAgain_2:
	cout << "\n                   Введите второй ключ: \n";
	getline(cin, number2, ';');
	if (size(number2) > 10 || size(number2) < 2) {
		cout << "\nВведите другой набор ключей: \n";
		goto tryAgain_2;
	}


	// перевод в бинарник первого ключа
	register int counter_1 = 1;
	string bit_test_1 = "";
	for (counter_1; counter_1 <= size(number1) - 1; counter_1++) {
		register int x = (int)(number1[counter_1]);
		bit_test_1 += bit_1_encrypt_1(x);
	}
	cout << "\nБинарный код первого ключа: " << bit_test_1 << "\n";

	// перевод в бинарник второго ключа
	register int counter_2 = 1;
	string bit_test_2 = "";
	for (counter_2; counter_2 <= size(number2) - 1; counter_2++) {
		register int x1 = (int)(number2[counter_2]);
		bit_test_2 += bit_2_encrypt_1(x1);
	}
	cout << "\nБинарный код второго ключа: " << bit_test_2 << "\n";


	cout << "\n                   Введите слово, которое требуется расшифровать: \n";
	getline(cin, word, ';');

	// перевод в бинарник шифруемого слова
	register int counter_3 = 1;
	string bit_test_3 = "";
	for (counter_3; counter_3 <= size(word) - 1; counter_3++) {
		register int x2 = (int)(word[counter_3]);
		bit_test_3 += bit_3_encrypt_1(x2);
	}
	cout << "\nБинарный код введённого шифрованного слова: " << bit_test_3 << "\n";

	register string beatstream;
	register char null_element1;
	register char null_element2;

	//цикл для создания битового потока, который будет шифровать 
	for (register int counter = 0; counter < size(bit_test_3); counter++) {
		register string beatstream_bit;
		stringstream ss;

		switch (size(bit_test_1))
		{

		case 2:
		{
			if (bit_test_1[0] == '1' && bit_test_1[1] == '1' || bit_test_1[0] == '0' && bit_test_1[1] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 3:
		{
			if (bit_test_1[1] == '1' && bit_test_1[2] == '1' || bit_test_1[1] == '0' && bit_test_1[2] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 4:
		{
			if (bit_test_1[3] == '1' && bit_test_1[2] == '1' || bit_test_1[3] == '0' && bit_test_1[2] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 5:
		{
			if (bit_test_1[2] == '1' && bit_test_1[4] == '1' || bit_test_1[2] == '0' && bit_test_1[4] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 6:
		{
			if (bit_test_1[5] == '1' && bit_test_1[4] == '1' || bit_test_1[5] == '0' && bit_test_1[4] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 7:
		{
			if (bit_test_1[3] == '1' && bit_test_1[6] == '1' || bit_test_1[3] == '0' && bit_test_1[6] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 8:
		{
			if (bit_test_1[3] == '0' && bit_test_1[4] == '0' && bit_test_1[5] == '0' && bit_test_1[7] == '0' || bit_test_1[3] == '0' && bit_test_1[4] == '0' && bit_test_1[5] == '1' && bit_test_1[7] == '1' || bit_test_1[3] == '0' && bit_test_1[4] == '1' && bit_test_1[5] == '0' && bit_test_1[7] == '1' || bit_test_1[3] == '0' && bit_test_1[4] == '1' && bit_test_1[5] == '1' && bit_test_1[7] == '0' || bit_test_1[3] == '1' && bit_test_1[4] == '0' && bit_test_1[5] == '0' && bit_test_1[7] == '1' || bit_test_1[3] == '1' && bit_test_1[4] == '0' && bit_test_1[5] == '1' && bit_test_1[7] == '0' || bit_test_1[3] == '1' && bit_test_1[4] == '1' && bit_test_1[5] == '0' && bit_test_1[7] == '0' || bit_test_1[3] == '1' && bit_test_1[4] == '1' && bit_test_1[5] == '1' && bit_test_1[7] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 9:
		{
			if (bit_test_1[4] == '1' && bit_test_1[8] == '1' || bit_test_1[4] == '0' && bit_test_1[8] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 10:
		{
			if (bit_test_1[6] == '1' && bit_test_1[9] == '1' || bit_test_1[6] == '0' && bit_test_1[9] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 11:
		{
			if (bit_test_1[8] == '1' && bit_test_1[10] == '1' || bit_test_1[8] == '0' && bit_test_1[10] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 12:
		{
			if (bit_test_1[5] == '0' && bit_test_1[7] == '0' && bit_test_1[10] == '0' && bit_test_1[11] == '0' || bit_test_1[5] == '0' && bit_test_1[7] == '0' && bit_test_1[10] == '1' && bit_test_1[11] == '1' || bit_test_1[5] == '0' && bit_test_1[7] == '1' && bit_test_1[10] == '0' && bit_test_1[11] == '1' || bit_test_1[5] == '0' && bit_test_1[7] == '1' && bit_test_1[10] == '1' && bit_test_1[11] == '0' || bit_test_1[5] == '1' && bit_test_1[7] == '0' && bit_test_1[10] == '0' && bit_test_1[11] == '1' || bit_test_1[5] == '1' && bit_test_1[7] == '0' && bit_test_1[10] == '1' && bit_test_1[11] == '0' || bit_test_1[5] == '1' && bit_test_1[7] == '1' && bit_test_1[10] == '0' && bit_test_1[11] == '0' || bit_test_1[5] == '1' && bit_test_1[7] == '1' && bit_test_1[10] == '1' && bit_test_1[11] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 13:
		{
			if (bit_test_1[8] == '0' && bit_test_1[9] == '0' && bit_test_1[11] == '0' && bit_test_1[12] == '0' || bit_test_1[8] == '0' && bit_test_1[9] == '0' && bit_test_1[11] == '1' && bit_test_1[12] == '1' || bit_test_1[8] == '0' && bit_test_1[9] == '1' && bit_test_1[11] == '0' && bit_test_1[12] == '1' || bit_test_1[8] == '0' && bit_test_1[9] == '1' && bit_test_1[11] == '1' && bit_test_1[12] == '0' || bit_test_1[8] == '1' && bit_test_1[9] == '0' && bit_test_1[11] == '0' && bit_test_1[12] == '1' || bit_test_1[8] == '1' && bit_test_1[9] == '0' && bit_test_1[11] == '1' && bit_test_1[12] == '0' || bit_test_1[8] == '1' && bit_test_1[9] == '1' && bit_test_1[11] == '0' && bit_test_1[12] == '0' || bit_test_1[8] == '1' && bit_test_1[9] == '1' && bit_test_1[11] == '1' && bit_test_1[12] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 14:
		{

			if (bit_test_1[3] == '0' && bit_test_1[7] == '0' && bit_test_1[12] == '0' && bit_test_1[13] == '0' || bit_test_1[3] == '0' && bit_test_1[7] == '0' && bit_test_1[12] == '1' && bit_test_1[13] == '1' || bit_test_1[3] == '0' && bit_test_1[7] == '1' && bit_test_1[12] == '0' && bit_test_1[13] == '1' || bit_test_1[3] == '0' && bit_test_1[7] == '1' && bit_test_1[12] == '1' && bit_test_1[13] == '0' || bit_test_1[3] == '1' && bit_test_1[7] == '0' && bit_test_1[12] == '0' && bit_test_1[13] == '1' || bit_test_1[3] == '1' && bit_test_1[7] == '0' && bit_test_1[12] == '1' && bit_test_1[13] == '0' || bit_test_1[3] == '1' && bit_test_1[7] == '1' && bit_test_1[12] == '0' && bit_test_1[13] == '0' || bit_test_1[3] == '1' && bit_test_1[7] == '1' && bit_test_1[12] == '1' && bit_test_1[13] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 15:
		{
			if (bit_test_1[14] == '1' && bit_test_1[13] == '1' || bit_test_1[14] == '0' && bit_test_1[13] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 16:
		{
			if (bit_test_1[3] == '0' && bit_test_1[14] == '0' && bit_test_1[12] == '0' && bit_test_1[15] == '0' || bit_test_1[3] == '0' && bit_test_1[14] == '0' && bit_test_1[12] == '1' && bit_test_1[15] == '1' || bit_test_1[3] == '0' && bit_test_1[14] == '1' && bit_test_1[12] == '0' && bit_test_1[15] == '1' || bit_test_1[3] == '0' && bit_test_1[14] == '1' && bit_test_1[12] == '1' && bit_test_1[15] == '0' || bit_test_1[3] == '1' && bit_test_1[14] == '0' && bit_test_1[12] == '0' && bit_test_1[15] == '1' || bit_test_1[3] == '1' && bit_test_1[14] == '0' && bit_test_1[12] == '1' && bit_test_1[15] == '0' || bit_test_1[3] == '1' && bit_test_1[14] == '1' && bit_test_1[12] == '0' && bit_test_1[15] == '0' || bit_test_1[3] == '1' && bit_test_1[14] == '1' && bit_test_1[12] == '1' && bit_test_1[15] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 17:
		{
			if (bit_test_1[16] == '1' && bit_test_1[13] == '1' || bit_test_1[16] == '0' && bit_test_1[13] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 18:
		{
			if (bit_test_1[10] == '1' && bit_test_1[17] == '1' || bit_test_1[10] == '0' && bit_test_1[17] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 19:
		{
			if (bit_test_1[13] == '0' && bit_test_1[16] == '0' && bit_test_1[17] == '0' && bit_test_1[18] == '0' || bit_test_1[13] == '0' && bit_test_1[16] == '0' && bit_test_1[17] == '1' && bit_test_1[18] == '1' || bit_test_1[13] == '0' && bit_test_1[16] == '1' && bit_test_1[17] == '0' && bit_test_1[18] == '1' || bit_test_1[13] == '0' && bit_test_1[16] == '1' && bit_test_1[17] == '1' && bit_test_1[18] == '0' || bit_test_1[13] == '1' && bit_test_1[16] == '0' && bit_test_1[17] == '0' && bit_test_1[18] == '1' || bit_test_1[13] == '1' && bit_test_1[16] == '0' && bit_test_1[17] == '1' && bit_test_1[18] == '0' || bit_test_1[13] == '1' && bit_test_1[16] == '1' && bit_test_1[17] == '0' && bit_test_1[18] == '0' || bit_test_1[13] == '1' && bit_test_1[16] == '1' && bit_test_1[17] == '1' && bit_test_1[18] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 20:
		{
			if (bit_test_1[16] == '1' && bit_test_1[19] == '1' || bit_test_1[16] == '0' && bit_test_1[19] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 21:
		{
			if (bit_test_1[18] == '1' && bit_test_1[20] == '1' || bit_test_1[18] == '0' && bit_test_1[20] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 22:
		{
			if (bit_test_1[21] == '1' && bit_test_1[20] == '1' || bit_test_1[21] == '0' && bit_test_1[20] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 23:
		{
			if (bit_test_1[17] == '1' && bit_test_1[22] == '1' || bit_test_1[17] == '0' && bit_test_1[22] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 24:
		{
			if (bit_test_1[21] == '0' && bit_test_1[16] == '0' && bit_test_1[22] == '0' && bit_test_1[23] == '0' || bit_test_1[21] == '0' && bit_test_1[16] == '0' && bit_test_1[22] == '1' && bit_test_1[23] == '1' || bit_test_1[21] == '0' && bit_test_1[16] == '1' && bit_test_1[22] == '0' && bit_test_1[23] == '1' || bit_test_1[21] == '0' && bit_test_1[16] == '1' && bit_test_1[22] == '1' && bit_test_1[23] == '0' || bit_test_1[16] == '1' && bit_test_1[21] == '0' && bit_test_1[22] == '0' && bit_test_1[23] == '1' || bit_test_1[21] == '1' && bit_test_1[16] == '0' && bit_test_1[22] == '1' && bit_test_1[23] == '0' || bit_test_1[21] == '1' && bit_test_1[16] == '1' && bit_test_1[22] == '0' && bit_test_1[23] == '0' || bit_test_1[21] == '1' && bit_test_1[16] == '1' && bit_test_1[22] == '1' && bit_test_1[23] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 25:
		{
			if (bit_test_1[21] == '1' && bit_test_1[24] == '1' || bit_test_1[21] == '0' && bit_test_1[24] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 26:
		{
			if (bit_test_1[19] == '0' && bit_test_1[24] == '0' && bit_test_1[25] == '0' && bit_test_1[23] == '0' || bit_test_1[24] == '0' && bit_test_1[19] == '0' && bit_test_1[25] == '1' && bit_test_1[23] == '1' || bit_test_1[24] == '0' && bit_test_1[19] == '1' && bit_test_1[25] == '0' && bit_test_1[23] == '1' || bit_test_1[24] == '0' && bit_test_1[19] == '1' && bit_test_1[25] == '1' && bit_test_1[23] == '0' || bit_test_1[19] == '1' && bit_test_1[24] == '0' && bit_test_1[25] == '0' && bit_test_1[23] == '1' || bit_test_1[24] == '1' && bit_test_1[19] == '0' && bit_test_1[25] == '1' && bit_test_1[23] == '0' || bit_test_1[24] == '1' && bit_test_1[19] == '1' && bit_test_1[25] == '0' && bit_test_1[23] == '0' || bit_test_1[24] == '1' && bit_test_1[19] == '1' && bit_test_1[25] == '1' && bit_test_1[23] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 27:
		{
			if (bit_test_1[21] == '0' && bit_test_1[24] == '0' && bit_test_1[25] == '0' && bit_test_1[26] == '0' || bit_test_1[24] == '0' && bit_test_1[21] == '0' && bit_test_1[25] == '1' && bit_test_1[26] == '1' || bit_test_1[24] == '0' && bit_test_1[21] == '1' && bit_test_1[25] == '0' && bit_test_1[26] == '1' || bit_test_1[24] == '0' && bit_test_1[21] == '1' && bit_test_1[25] == '1' && bit_test_1[26] == '0' || bit_test_1[21] == '1' && bit_test_1[24] == '0' && bit_test_1[25] == '0' && bit_test_1[26] == '1' || bit_test_1[24] == '1' && bit_test_1[21] == '0' && bit_test_1[25] == '1' && bit_test_1[26] == '0' || bit_test_1[24] == '1' && bit_test_1[21] == '1' && bit_test_1[25] == '0' && bit_test_1[26] == '0' || bit_test_1[24] == '1' && bit_test_1[21] == '1' && bit_test_1[25] == '1' && bit_test_1[26] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 28:
		{
			if (bit_test_1[27] == '1' && bit_test_1[24] == '1' || bit_test_1[27] == '0' && bit_test_1[24] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 29:
		{
			if (bit_test_1[26] == '1' && bit_test_1[28] == '1' || bit_test_1[26] == '0' && bit_test_1[28] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 30:
		{
			if (bit_test_1[6] == '0' && bit_test_1[27] == '0' && bit_test_1[28] == '0' && bit_test_1[29] == '0' || bit_test_1[6] == '0' && bit_test_1[27] == '0' && bit_test_1[28] == '1' && bit_test_1[29] == '1' || bit_test_1[6] == '0' && bit_test_1[27] == '1' && bit_test_1[28] == '0' && bit_test_1[29] == '1' || bit_test_1[6] == '0' && bit_test_1[27] == '1' && bit_test_1[28] == '1' && bit_test_1[29] == '0' || bit_test_1[6] == '1' && bit_test_1[27] == '0' && bit_test_1[28] == '0' && bit_test_1[29] == '1' || bit_test_1[6] == '1' && bit_test_1[27] == '0' && bit_test_1[28] == '1' && bit_test_1[29] == '0' || bit_test_1[6] == '1' && bit_test_1[27] == '1' && bit_test_1[28] == '0' && bit_test_1[29] == '0' || bit_test_1[6] == '1' && bit_test_1[27] == '1' && bit_test_1[28] == '1' && bit_test_1[29] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 31:
		{
			if (bit_test_1[27] == '1' && bit_test_1[30] == '1' || bit_test_1[27] == '0' && bit_test_1[30] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 32:
		{
			if (bit_test_1[9] == '0' && bit_test_1[30] == '0' && bit_test_1[31] == '0' && bit_test_1[29] == '0' || bit_test_1[9] == '0' && bit_test_1[30] == '0' && bit_test_1[31] == '1' && bit_test_1[29] == '1' || bit_test_1[9] == '0' && bit_test_1[31] == '1' && bit_test_1[30] == '0' && bit_test_1[29] == '1' || bit_test_1[9] == '0' && bit_test_1[30] == '1' && bit_test_1[31] == '1' && bit_test_1[29] == '0' || bit_test_1[9] == '1' && bit_test_1[30] == '0' && bit_test_1[31] == '0' && bit_test_1[29] == '1' || bit_test_1[9] == '1' && bit_test_1[30] == '0' && bit_test_1[31] == '1' && bit_test_1[29] == '0' || bit_test_1[9] == '1' && bit_test_1[30] == '1' && bit_test_1[31] == '0' && bit_test_1[29] == '0' || bit_test_1[9] == '1' && bit_test_1[30] == '1' && bit_test_1[31] == '1' && bit_test_1[29] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 33:
		{
			if (bit_test_1[19] == '1' && bit_test_1[32] == '1' || bit_test_1[19] == '0' && bit_test_1[32] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 34:
		{
			if (bit_test_1[29] == '0' && bit_test_1[30] == '0' && bit_test_1[33] == '0' && bit_test_1[25] == '0' || bit_test_1[25] == '0' && bit_test_1[30] == '0' && bit_test_1[33] == '1' && bit_test_1[29] == '1' || bit_test_1[25] == '0' && bit_test_1[29] == '1' && bit_test_1[30] == '0' && bit_test_1[33] == '1' || bit_test_1[25] == '0' && bit_test_1[30] == '1' && bit_test_1[33] == '1' && bit_test_1[29] == '0' || bit_test_1[25] == '1' && bit_test_1[30] == '0' && bit_test_1[33] == '0' && bit_test_1[29] == '1' || bit_test_1[25] == '1' && bit_test_1[30] == '0' && bit_test_1[33] == '1' && bit_test_1[29] == '0' || bit_test_1[25] == '1' && bit_test_1[30] == '1' && bit_test_1[33] == '0' && bit_test_1[29] == '0' || bit_test_1[25] == '1' && bit_test_1[30] == '1' && bit_test_1[33] == '1' && bit_test_1[29] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 35:
		{
			if (bit_test_1[34] == '1' && bit_test_1[32] == '1' || bit_test_1[34] == '0' && bit_test_1[32] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 36:
		{
			if (bit_test_1[24] == '1' && bit_test_1[35] == '1' || bit_test_1[24] == '0' && bit_test_1[35] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 37:
		{
			if (bit_test_1[32] == '0' && bit_test_1[30] == '0' && bit_test_1[36] == '0' && bit_test_1[35] == '0' || bit_test_1[30] == '0' && bit_test_1[32] == '0' && bit_test_1[35] == '1' && bit_test_1[36] == '1' || bit_test_1[30] == '0' && bit_test_1[32] == '1' && bit_test_1[35] == '0' && bit_test_1[36] == '1' || bit_test_1[30] == '0' && bit_test_1[32] == '1' && bit_test_1[35] == '1' && bit_test_1[36] == '0' || bit_test_1[30] == '1' && bit_test_1[32] == '0' && bit_test_1[35] == '0' && bit_test_1[36] == '1' || bit_test_1[30] == '1' && bit_test_1[32] == '0' && bit_test_1[35] == '1' && bit_test_1[36] == '0' || bit_test_1[30] == '1' && bit_test_1[32] == '1' && bit_test_1[35] == '0' && bit_test_1[36] == '0' || bit_test_1[30] == '1' && bit_test_1[32] == '1' && bit_test_1[35] == '1' && bit_test_1[36] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 38:
		{
			if (bit_test_1[32] == '0' && bit_test_1[31] == '0' && bit_test_1[36] == '0' && bit_test_1[37] == '0' || bit_test_1[31] == '0' && bit_test_1[32] == '0' && bit_test_1[37] == '1' && bit_test_1[36] == '1' || bit_test_1[31] == '0' && bit_test_1[32] == '1' && bit_test_1[37] == '0' && bit_test_1[36] == '1' || bit_test_1[31] == '0' && bit_test_1[32] == '1' && bit_test_1[37] == '1' && bit_test_1[36] == '0' || bit_test_1[31] == '1' && bit_test_1[32] == '0' && bit_test_1[37] == '0' && bit_test_1[36] == '1' || bit_test_1[31] == '1' && bit_test_1[32] == '0' && bit_test_1[37] == '1' && bit_test_1[36] == '0' || bit_test_1[31] == '1' && bit_test_1[32] == '1' && bit_test_1[37] == '0' && bit_test_1[36] == '0' || bit_test_1[31] == '1' && bit_test_1[32] == '1' && bit_test_1[37] == '1' && bit_test_1[36] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 39:
		{
			if (bit_test_1[34] == '1' && bit_test_1[38] == '1' || bit_test_1[34] == '0' && bit_test_1[38] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 40:
		{
			if (bit_test_1[34] == '0' && bit_test_1[35] == '0' && bit_test_1[36] == '0' && bit_test_1[39] == '0' || bit_test_1[34] == '0' && bit_test_1[35] == '0' && bit_test_1[39] == '1' && bit_test_1[36] == '1' || bit_test_1[34] == '0' && bit_test_1[35] == '1' && bit_test_1[39] == '0' && bit_test_1[36] == '1' || bit_test_1[34] == '0' && bit_test_1[35] == '1' && bit_test_1[39] == '1' && bit_test_1[36] == '0' || bit_test_1[34] == '1' && bit_test_1[35] == '0' && bit_test_1[39] == '0' && bit_test_1[36] == '1' || bit_test_1[34] == '1' && bit_test_1[35] == '0' && bit_test_1[39] == '1' && bit_test_1[36] == '0' || bit_test_1[34] == '1' && bit_test_1[35] == '1' && bit_test_1[39] == '0' && bit_test_1[36] == '0' || bit_test_1[34] == '1' && bit_test_1[35] == '1' && bit_test_1[39] == '1' && bit_test_1[36] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 41:
		{
			if (bit_test_1[37] == '1' && bit_test_1[40] == '1' || bit_test_1[37] == '0' && bit_test_1[40] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 42:
		{
			if (bit_test_1[34] == '0' && bit_test_1[37] == '0' && bit_test_1[38] == '0' && bit_test_1[41] == '0' || bit_test_1[34] == '0' && bit_test_1[37] == '0' && bit_test_1[38] == '1' && bit_test_1[41] == '1' || bit_test_1[34] == '0' && bit_test_1[37] == '1' && bit_test_1[38] == '0' && bit_test_1[41] == '1' || bit_test_1[34] == '0' && bit_test_1[37] == '1' && bit_test_1[38] == '1' && bit_test_1[41] == '0' || bit_test_1[34] == '1' && bit_test_1[37] == '0' && bit_test_1[38] == '0' && bit_test_1[41] == '1' || bit_test_1[34] == '1' && bit_test_1[37] == '0' && bit_test_1[38] == '1' && bit_test_1[41] == '0' || bit_test_1[34] == '1' && bit_test_1[37] == '1' && bit_test_1[38] == '0' && bit_test_1[41] == '0' || bit_test_1[34] == '1' && bit_test_1[37] == '1' && bit_test_1[38] == '1' && bit_test_1[41] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 43:
		{
			if (bit_test_1[36] == '0' && bit_test_1[39] == '0' && bit_test_1[38] == '0' && bit_test_1[42] == '0' || bit_test_1[36] == '0' && bit_test_1[39] == '0' && bit_test_1[38] == '1' && bit_test_1[42] == '1' || bit_test_1[36] == '0' && bit_test_1[39] == '1' && bit_test_1[38] == '0' && bit_test_1[42] == '1' || bit_test_1[36] == '0' && bit_test_1[39] == '1' && bit_test_1[38] == '1' && bit_test_1[42] == '0' || bit_test_1[36] == '1' && bit_test_1[39] == '0' && bit_test_1[38] == '0' && bit_test_1[42] == '1' || bit_test_1[36] == '1' && bit_test_1[39] == '0' && bit_test_1[38] == '1' && bit_test_1[42] == '0' || bit_test_1[36] == '1' && bit_test_1[39] == '1' && bit_test_1[38] == '0' && bit_test_1[42] == '0' || bit_test_1[36] == '1' && bit_test_1[39] == '1' && bit_test_1[38] == '1' && bit_test_1[42] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 44:
		{
			if (bit_test_1[43] == '0' && bit_test_1[37] == '0' && bit_test_1[38] == '0' && bit_test_1[41] == '0' || bit_test_1[43] == '0' && bit_test_1[37] == '0' && bit_test_1[38] == '1' && bit_test_1[41] == '1' || bit_test_1[43] == '0' && bit_test_1[37] == '1' && bit_test_1[38] == '0' && bit_test_1[41] == '1' || bit_test_1[43] == '0' && bit_test_1[37] == '1' && bit_test_1[38] == '1' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[37] == '0' && bit_test_1[38] == '0' && bit_test_1[41] == '1' || bit_test_1[43] == '1' && bit_test_1[37] == '0' && bit_test_1[38] == '1' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[37] == '1' && bit_test_1[38] == '0' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[37] == '1' && bit_test_1[38] == '1' && bit_test_1[41] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 45:
		{
			if (bit_test_1[43] == '0' && bit_test_1[40] == '0' && bit_test_1[44] == '0' && bit_test_1[41] == '0' || bit_test_1[43] == '0' && bit_test_1[40] == '0' && bit_test_1[44] == '1' && bit_test_1[41] == '1' || bit_test_1[43] == '0' && bit_test_1[40] == '1' && bit_test_1[44] == '0' && bit_test_1[41] == '1' || bit_test_1[43] == '0' && bit_test_1[40] == '1' && bit_test_1[44] == '1' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '0' && bit_test_1[44] == '0' && bit_test_1[41] == '1' || bit_test_1[43] == '1' && bit_test_1[40] == '0' && bit_test_1[44] == '1' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '1' && bit_test_1[44] == '0' && bit_test_1[41] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '1' && bit_test_1[44] == '1' && bit_test_1[41] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 46:
		{
			if (bit_test_1[37] == '0' && bit_test_1[39] == '0' && bit_test_1[38] == '0' && bit_test_1[45] == '0' || bit_test_1[37] == '0' && bit_test_1[39] == '0' && bit_test_1[38] == '1' && bit_test_1[45] == '1' || bit_test_1[37] == '0' && bit_test_1[39] == '1' && bit_test_1[38] == '0' && bit_test_1[45] == '1' || bit_test_1[37] == '0' && bit_test_1[39] == '1' && bit_test_1[38] == '1' && bit_test_1[45] == '0' || bit_test_1[37] == '1' && bit_test_1[39] == '0' && bit_test_1[38] == '0' && bit_test_1[45] == '1' || bit_test_1[37] == '1' && bit_test_1[39] == '0' && bit_test_1[38] == '1' && bit_test_1[45] == '0' || bit_test_1[37] == '1' && bit_test_1[39] == '1' && bit_test_1[38] == '0' && bit_test_1[45] == '0' || bit_test_1[37] == '1' && bit_test_1[39] == '1' && bit_test_1[38] == '1' && bit_test_1[45] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 47:
		{
			if (bit_test_1[41] == '1' && bit_test_1[46] == '1' || bit_test_1[41] == '0' && bit_test_1[46] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 48:
		{
			if (bit_test_1[43] == '0' && bit_test_1[40] == '0' && bit_test_1[38] == '0' && bit_test_1[47] == '0' || bit_test_1[43] == '0' && bit_test_1[40] == '0' && bit_test_1[38] == '1' && bit_test_1[47] == '1' || bit_test_1[43] == '0' && bit_test_1[40] == '1' && bit_test_1[38] == '0' && bit_test_1[47] == '1' || bit_test_1[43] == '0' && bit_test_1[40] == '1' && bit_test_1[38] == '1' && bit_test_1[47] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '0' && bit_test_1[38] == '0' && bit_test_1[47] == '1' || bit_test_1[43] == '1' && bit_test_1[40] == '0' && bit_test_1[38] == '1' && bit_test_1[47] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '1' && bit_test_1[38] == '0' && bit_test_1[47] == '0' || bit_test_1[43] == '1' && bit_test_1[40] == '1' && bit_test_1[38] == '1' && bit_test_1[47] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 49:
		{
			if (bit_test_1[39] == '1' && bit_test_1[48] == '1' || bit_test_1[39] == '0' && bit_test_1[48] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 50:
		{
			if (bit_test_1[45] == '0' && bit_test_1[46] == '0' && bit_test_1[47] == '0' && bit_test_1[49] == '0' || bit_test_1[45] == '0' && bit_test_1[46] == '0' && bit_test_1[47] == '1' && bit_test_1[49] == '1' || bit_test_1[45] == '0' && bit_test_1[46] == '1' && bit_test_1[47] == '0' && bit_test_1[49] == '1' || bit_test_1[45] == '0' && bit_test_1[46] == '1' && bit_test_1[47] == '1' && bit_test_1[49] == '0' || bit_test_1[45] == '1' && bit_test_1[46] == '0' && bit_test_1[47] == '0' && bit_test_1[49] == '1' || bit_test_1[45] == '1' && bit_test_1[46] == '0' && bit_test_1[47] == '1' && bit_test_1[49] == '0' || bit_test_1[45] == '1' && bit_test_1[46] == '1' && bit_test_1[47] == '0' && bit_test_1[49] == '0' || bit_test_1[45] == '1' && bit_test_1[46] == '1' && bit_test_1[47] == '1' && bit_test_1[49] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 51:
		{
			if (bit_test_1[44] == '0' && bit_test_1[50] == '0' && bit_test_1[47] == '0' && bit_test_1[49] == '0' || bit_test_1[44] == '0' && bit_test_1[50] == '0' && bit_test_1[47] == '1' && bit_test_1[49] == '1' || bit_test_1[44] == '0' && bit_test_1[50] == '1' && bit_test_1[47] == '0' && bit_test_1[49] == '1' || bit_test_1[44] == '0' && bit_test_1[50] == '1' && bit_test_1[47] == '1' && bit_test_1[49] == '0' || bit_test_1[44] == '1' && bit_test_1[50] == '0' && bit_test_1[47] == '0' && bit_test_1[49] == '1' || bit_test_1[44] == '1' && bit_test_1[50] == '0' && bit_test_1[47] == '1' && bit_test_1[49] == '0' || bit_test_1[44] == '1' && bit_test_1[50] == '1' && bit_test_1[47] == '0' && bit_test_1[49] == '0' || bit_test_1[44] == '1' && bit_test_1[50] == '1' && bit_test_1[47] == '1' && bit_test_1[49] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 52:
		{
			if (bit_test_1[51] == '1' && bit_test_1[48] == '1' || bit_test_1[51] == '0' && bit_test_1[48] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 53:
		{
			if (bit_test_1[50] == '0' && bit_test_1[46] == '0' && bit_test_1[51] == '0' && bit_test_1[52] == '0' || bit_test_1[50] == '0' && bit_test_1[46] == '0' && bit_test_1[51] == '1' && bit_test_1[52] == '1' || bit_test_1[50] == '0' && bit_test_1[46] == '1' && bit_test_1[51] == '0' && bit_test_1[52] == '1' || bit_test_1[50] == '0' && bit_test_1[46] == '1' && bit_test_1[51] == '1' && bit_test_1[52] == '0' || bit_test_1[50] == '1' && bit_test_1[46] == '0' && bit_test_1[51] == '0' && bit_test_1[52] == '1' || bit_test_1[50] == '1' && bit_test_1[46] == '0' && bit_test_1[51] == '1' && bit_test_1[52] == '0' || bit_test_1[50] == '1' && bit_test_1[46] == '1' && bit_test_1[51] == '0' && bit_test_1[52] == '0' || bit_test_1[50] == '1' && bit_test_1[46] == '1' && bit_test_1[51] == '1' && bit_test_1[52] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 54:
		{
			if (bit_test_1[45] == '0' && bit_test_1[50] == '0' && bit_test_1[47] == '0' && bit_test_1[53] == '0' || bit_test_1[45] == '0' && bit_test_1[50] == '0' && bit_test_1[47] == '1' && bit_test_1[53] == '1' || bit_test_1[45] == '0' && bit_test_1[50] == '1' && bit_test_1[47] == '0' && bit_test_1[53] == '1' || bit_test_1[45] == '0' && bit_test_1[50] == '1' && bit_test_1[47] == '1' && bit_test_1[53] == '0' || bit_test_1[45] == '1' && bit_test_1[50] == '0' && bit_test_1[47] == '0' && bit_test_1[53] == '1' || bit_test_1[45] == '1' && bit_test_1[50] == '0' && bit_test_1[47] == '1' && bit_test_1[53] == '0' || bit_test_1[45] == '1' && bit_test_1[50] == '1' && bit_test_1[47] == '0' && bit_test_1[53] == '0' || bit_test_1[45] == '1' && bit_test_1[50] == '1' && bit_test_1[47] == '1' && bit_test_1[53] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 55:
		{
			if (bit_test_1[54] == '1' && bit_test_1[30] == '1' || bit_test_1[54] == '0' && bit_test_1[30] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 56:
		{
			if (bit_test_1[51] == '0' && bit_test_1[48] == '0' && bit_test_1[53] == '0' && bit_test_1[55] == '0' || bit_test_1[51] == '0' && bit_test_1[48] == '0' && bit_test_1[53] == '1' && bit_test_1[55] == '1' || bit_test_1[51] == '0' && bit_test_1[48] == '1' && bit_test_1[53] == '0' && bit_test_1[55] == '1' || bit_test_1[51] == '0' && bit_test_1[48] == '1' && bit_test_1[53] == '1' && bit_test_1[55] == '0' || bit_test_1[51] == '1' && bit_test_1[48] == '0' && bit_test_1[53] == '0' && bit_test_1[55] == '1' || bit_test_1[51] == '1' && bit_test_1[48] == '0' && bit_test_1[53] == '1' && bit_test_1[55] == '0' || bit_test_1[51] == '1' && bit_test_1[48] == '1' && bit_test_1[53] == '0' && bit_test_1[55] == '0' || bit_test_1[51] == '1' && bit_test_1[48] == '1' && bit_test_1[53] == '1' && bit_test_1[55] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 57:
		{
			if (bit_test_1[56] == '1' && bit_test_1[49] == '1' || bit_test_1[56] == '0' && bit_test_1[49] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 58:
		{
			if (bit_test_1[57] == '1' && bit_test_1[38] == '1' || bit_test_1[57] == '0' && bit_test_1[38] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 59:
		{
			if (bit_test_1[54] == '0' && bit_test_1[56] == '0' && bit_test_1[51] == '0' && bit_test_1[58] == '0' || bit_test_1[54] == '0' && bit_test_1[56] == '0' && bit_test_1[51] == '1' && bit_test_1[58] == '1' || bit_test_1[54] == '0' && bit_test_1[56] == '1' && bit_test_1[51] == '0' && bit_test_1[58] == '1' || bit_test_1[54] == '0' && bit_test_1[56] == '1' && bit_test_1[51] == '1' && bit_test_1[58] == '0' || bit_test_1[54] == '1' && bit_test_1[56] == '0' && bit_test_1[51] == '0' && bit_test_1[58] == '1' || bit_test_1[54] == '1' && bit_test_1[56] == '0' && bit_test_1[51] == '1' && bit_test_1[58] == '0' || bit_test_1[54] == '1' && bit_test_1[56] == '1' && bit_test_1[51] == '0' && bit_test_1[58] == '0' || bit_test_1[54] == '1' && bit_test_1[56] == '1' && bit_test_1[51] == '1' && bit_test_1[58] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 60:
		{
			if (bit_test_1[59] == '1' && bit_test_1[58] == '1' || bit_test_1[59] == '0' && bit_test_1[58] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 61:
		{
			if (bit_test_1[55] == '0' && bit_test_1[59] == '0' && bit_test_1[60] == '0' && bit_test_1[58] == '0' || bit_test_1[55] == '0' && bit_test_1[59] == '0' && bit_test_1[60] == '1' && bit_test_1[58] == '1' || bit_test_1[55] == '0' && bit_test_1[59] == '1' && bit_test_1[60] == '0' && bit_test_1[58] == '1' || bit_test_1[55] == '0' && bit_test_1[59] == '1' && bit_test_1[60] == '1' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[59] == '0' && bit_test_1[60] == '0' && bit_test_1[58] == '1' || bit_test_1[55] == '1' && bit_test_1[59] == '0' && bit_test_1[60] == '1' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[59] == '1' && bit_test_1[60] == '0' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[59] == '1' && bit_test_1[60] == '1' && bit_test_1[58] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 62:
		{
			if (bit_test_1[55] == '0' && bit_test_1[56] == '0' && bit_test_1[60] == '0' && bit_test_1[58] == '0' || bit_test_1[55] == '0' && bit_test_1[56] == '0' && bit_test_1[60] == '1' && bit_test_1[58] == '1' || bit_test_1[55] == '0' && bit_test_1[56] == '1' && bit_test_1[60] == '0' && bit_test_1[58] == '1' || bit_test_1[55] == '0' && bit_test_1[56] == '1' && bit_test_1[60] == '1' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[56] == '0' && bit_test_1[60] == '0' && bit_test_1[58] == '1' || bit_test_1[55] == '1' && bit_test_1[56] == '0' && bit_test_1[60] == '1' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[56] == '1' && bit_test_1[60] == '0' && bit_test_1[58] == '0' || bit_test_1[55] == '1' && bit_test_1[56] == '1' && bit_test_1[60] == '1' && bit_test_1[58] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 63:
		{
			if (bit_test_1[61] == '1' && bit_test_1[62] == '1' || bit_test_1[61] == '0' && bit_test_1[62] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 64:
		{
			if (bit_test_1[62] == '0' && bit_test_1[59] == '0' && bit_test_1[60] == '0' && bit_test_1[63] == '0' || bit_test_1[62] == '0' && bit_test_1[59] == '0' && bit_test_1[60] == '1' && bit_test_1[63] == '1' || bit_test_1[62] == '0' && bit_test_1[59] == '1' && bit_test_1[60] == '0' && bit_test_1[63] == '1' || bit_test_1[62] == '0' && bit_test_1[59] == '1' && bit_test_1[60] == '1' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[59] == '0' && bit_test_1[60] == '0' && bit_test_1[63] == '1' || bit_test_1[62] == '1' && bit_test_1[59] == '0' && bit_test_1[60] == '1' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[59] == '1' && bit_test_1[60] == '0' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[59] == '1' && bit_test_1[60] == '1' && bit_test_1[63] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 65:
		{
			if (bit_test_1[64] == '1' && bit_test_1[46] == '1' || bit_test_1[64] == '0' && bit_test_1[46] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 66:
		{
			if (bit_test_1[56] == '0' && bit_test_1[59] == '0' && bit_test_1[65] == '0' && bit_test_1[57] == '0' || bit_test_1[56] == '0' && bit_test_1[59] == '0' && bit_test_1[65] == '1' && bit_test_1[57] == '1' || bit_test_1[56] == '0' && bit_test_1[59] == '1' && bit_test_1[65] == '0' && bit_test_1[57] == '1' || bit_test_1[56] == '0' && bit_test_1[59] == '1' && bit_test_1[65] == '1' && bit_test_1[57] == '0' || bit_test_1[56] == '1' && bit_test_1[59] == '0' && bit_test_1[65] == '0' && bit_test_1[57] == '1' || bit_test_1[56] == '1' && bit_test_1[59] == '0' && bit_test_1[65] == '1' && bit_test_1[57] == '0' || bit_test_1[56] == '1' && bit_test_1[59] == '1' && bit_test_1[65] == '0' && bit_test_1[57] == '0' || bit_test_1[56] == '1' && bit_test_1[59] == '1' && bit_test_1[65] == '1' && bit_test_1[57] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 67:
		{
			if (bit_test_1[61] == '0' && bit_test_1[64] == '0' && bit_test_1[65] == '0' && bit_test_1[66] == '0' || bit_test_1[61] == '0' && bit_test_1[64] == '0' && bit_test_1[65] == '1' && bit_test_1[66] == '1' || bit_test_1[61] == '0' && bit_test_1[64] == '1' && bit_test_1[65] == '0' && bit_test_1[66] == '1' || bit_test_1[61] == '0' && bit_test_1[64] == '1' && bit_test_1[65] == '1' && bit_test_1[66] == '0' || bit_test_1[61] == '1' && bit_test_1[64] == '0' && bit_test_1[65] == '0' && bit_test_1[66] == '1' || bit_test_1[61] == '1' && bit_test_1[64] == '0' && bit_test_1[65] == '1' && bit_test_1[66] == '0' || bit_test_1[61] == '1' && bit_test_1[64] == '1' && bit_test_1[65] == '0' && bit_test_1[66] == '0' || bit_test_1[61] == '1' && bit_test_1[64] == '1' && bit_test_1[65] == '1' && bit_test_1[66] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 68:
		{
			if (bit_test_1[67] == '1' && bit_test_1[58] == '1' || bit_test_1[67] == '0' && bit_test_1[58] == '0') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 69:
		{
			if (bit_test_1[62] == '0' && bit_test_1[66] == '0' && bit_test_1[68] == '0' && bit_test_1[63] == '0' || bit_test_1[62] == '0' && bit_test_1[66] == '0' && bit_test_1[68] == '1' && bit_test_1[63] == '1' || bit_test_1[62] == '0' && bit_test_1[66] == '1' && bit_test_1[68] == '0' && bit_test_1[63] == '1' || bit_test_1[62] == '0' && bit_test_1[66] == '1' && bit_test_1[68] == '1' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[66] == '0' && bit_test_1[68] == '0' && bit_test_1[63] == '1' || bit_test_1[62] == '1' && bit_test_1[66] == '0' && bit_test_1[68] == '1' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[66] == '1' && bit_test_1[68] == '0' && bit_test_1[63] == '0' || bit_test_1[62] == '1' && bit_test_1[66] == '1' && bit_test_1[68] == '1' && bit_test_1[63] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		case 70:
		{
			if (bit_test_1[68] == '0' && bit_test_1[64] == '0' && bit_test_1[69] == '0' && bit_test_1[66] == '0' || bit_test_1[68] == '0' && bit_test_1[64] == '0' && bit_test_1[69] == '1' && bit_test_1[66] == '1' || bit_test_1[68] == '0' && bit_test_1[64] == '1' && bit_test_1[69] == '0' && bit_test_1[66] == '1' || bit_test_1[68] == '0' && bit_test_1[64] == '1' && bit_test_1[69] == '1' && bit_test_1[66] == '0' || bit_test_1[68] == '1' && bit_test_1[64] == '0' && bit_test_1[69] == '0' && bit_test_1[66] == '1' || bit_test_1[68] == '1' && bit_test_1[64] == '0' && bit_test_1[69] == '1' && bit_test_1[66] == '0' || bit_test_1[68] == '1' && bit_test_1[64] == '1' && bit_test_1[69] == '0' && bit_test_1[66] == '0' || bit_test_1[68] == '1' && bit_test_1[64] == '1' && bit_test_1[69] == '1' && bit_test_1[66] == '1') {
				null_element1 = '0';
			}
			else {
				null_element1 = '1';
			}
			rotate(bit_test_1.rbegin(), bit_test_1.rbegin() + 1, bit_test_1.rend());
			bit_test_1[0] = null_element1;
			break;
		}

		//add more number
		}




		switch (size(bit_test_2))
		{

		case 2:
		{
			if (bit_test_2[0] == '1' && bit_test_2[1] == '1' || bit_test_2[0] == '0' && bit_test_2[1] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 3:
		{
			if (bit_test_2[1] == '1' && bit_test_2[2] == '1' || bit_test_2[1] == '0' && bit_test_2[2] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 4:
		{
			if (bit_test_2[3] == '1' && bit_test_2[2] == '1' || bit_test_2[3] == '0' && bit_test_2[2] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 5:
		{
			if (bit_test_2[2] == '1' && bit_test_2[4] == '1' || bit_test_2[2] == '0' && bit_test_2[4] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 6:
		{
			if (bit_test_2[5] == '1' && bit_test_2[4] == '1' || bit_test_2[5] == '0' && bit_test_2[4] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 7:
		{
			if (bit_test_2[3] == '1' && bit_test_2[6] == '1' || bit_test_2[3] == '0' && bit_test_2[6] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 8:
		{
			if (bit_test_2[3] == '0' && bit_test_2[4] == '0' && bit_test_2[5] == '0' && bit_test_2[7] == '0' || bit_test_2[3] == '0' && bit_test_2[4] == '0' && bit_test_2[5] == '1' && bit_test_2[7] == '1' || bit_test_2[3] == '0' && bit_test_2[4] == '1' && bit_test_2[5] == '0' && bit_test_2[7] == '1' || bit_test_2[3] == '0' && bit_test_2[4] == '1' && bit_test_2[5] == '1' && bit_test_2[7] == '0' || bit_test_2[3] == '1' && bit_test_2[4] == '0' && bit_test_2[5] == '0' && bit_test_2[7] == '1' || bit_test_2[3] == '1' && bit_test_2[4] == '0' && bit_test_2[5] == '1' && bit_test_2[7] == '0' || bit_test_2[3] == '1' && bit_test_2[4] == '1' && bit_test_2[5] == '0' && bit_test_2[7] == '0' || bit_test_2[3] == '1' && bit_test_2[4] == '1' && bit_test_2[5] == '1' && bit_test_2[7] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 9:
		{
			if (bit_test_2[4] == '1' && bit_test_2[8] == '1' || bit_test_2[4] == '0' && bit_test_2[8] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 10:
		{
			if (bit_test_2[6] == '1' && bit_test_2[9] == '1' || bit_test_2[6] == '0' && bit_test_2[9] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 11:
		{
			if (bit_test_2[8] == '1' && bit_test_2[10] == '1' || bit_test_2[8] == '0' && bit_test_2[10] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 12:
		{
			if (bit_test_2[5] == '0' && bit_test_2[7] == '0' && bit_test_2[10] == '0' && bit_test_2[11] == '0' || bit_test_2[5] == '0' && bit_test_2[7] == '0' && bit_test_2[10] == '1' && bit_test_2[11] == '1' || bit_test_2[5] == '0' && bit_test_2[7] == '1' && bit_test_2[10] == '0' && bit_test_2[11] == '1' || bit_test_2[5] == '0' && bit_test_2[7] == '1' && bit_test_2[10] == '1' && bit_test_2[11] == '0' || bit_test_2[5] == '1' && bit_test_2[7] == '0' && bit_test_2[10] == '0' && bit_test_2[11] == '1' || bit_test_2[5] == '1' && bit_test_2[7] == '0' && bit_test_2[10] == '1' && bit_test_2[11] == '0' || bit_test_2[5] == '1' && bit_test_2[7] == '1' && bit_test_2[10] == '0' && bit_test_2[11] == '0' || bit_test_2[5] == '1' && bit_test_2[7] == '1' && bit_test_2[10] == '1' && bit_test_2[11] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 13:
		{
			if (bit_test_2[8] == '0' && bit_test_2[9] == '0' && bit_test_2[11] == '0' && bit_test_2[12] == '0' || bit_test_2[8] == '0' && bit_test_2[9] == '0' && bit_test_2[11] == '1' && bit_test_2[12] == '1' || bit_test_2[8] == '0' && bit_test_2[9] == '1' && bit_test_2[11] == '0' && bit_test_2[12] == '1' || bit_test_2[8] == '0' && bit_test_2[9] == '1' && bit_test_2[11] == '1' && bit_test_2[12] == '0' || bit_test_2[8] == '1' && bit_test_2[9] == '0' && bit_test_2[11] == '0' && bit_test_2[12] == '1' || bit_test_2[8] == '1' && bit_test_2[9] == '0' && bit_test_2[11] == '1' && bit_test_2[12] == '0' || bit_test_2[8] == '1' && bit_test_2[9] == '1' && bit_test_2[11] == '0' && bit_test_2[12] == '0' || bit_test_2[8] == '1' && bit_test_2[9] == '1' && bit_test_2[11] == '1' && bit_test_2[12] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 14:
		{
			if (bit_test_2[3] == '0' && bit_test_2[7] == '0' && bit_test_2[12] == '0' && bit_test_2[13] == '0' || bit_test_2[3] == '0' && bit_test_2[7] == '0' && bit_test_2[12] == '1' && bit_test_2[13] == '1' || bit_test_2[3] == '0' && bit_test_2[7] == '1' && bit_test_2[12] == '0' && bit_test_2[13] == '1' || bit_test_2[3] == '0' && bit_test_2[7] == '1' && bit_test_2[12] == '1' && bit_test_2[13] == '0' || bit_test_2[3] == '1' && bit_test_2[7] == '0' && bit_test_2[12] == '0' && bit_test_2[13] == '1' || bit_test_2[3] == '1' && bit_test_2[7] == '0' && bit_test_2[12] == '1' && bit_test_2[13] == '0' || bit_test_2[3] == '1' && bit_test_2[7] == '1' && bit_test_2[12] == '0' && bit_test_2[13] == '0' || bit_test_2[3] == '1' && bit_test_2[7] == '1' && bit_test_2[12] == '1' && bit_test_2[13] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 15:
		{
			if (bit_test_2[14] == '1' && bit_test_2[13] == '1' || bit_test_2[14] == '0' && bit_test_2[13] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 16:
		{
			if (bit_test_2[3] == '0' && bit_test_2[14] == '0' && bit_test_2[12] == '0' && bit_test_2[15] == '0' || bit_test_2[3] == '0' && bit_test_2[14] == '0' && bit_test_2[12] == '1' && bit_test_2[15] == '1' || bit_test_2[3] == '0' && bit_test_2[14] == '1' && bit_test_2[12] == '0' && bit_test_2[15] == '1' || bit_test_2[3] == '0' && bit_test_2[14] == '1' && bit_test_2[12] == '1' && bit_test_2[15] == '0' || bit_test_2[3] == '1' && bit_test_2[14] == '0' && bit_test_2[12] == '0' && bit_test_2[15] == '1' || bit_test_2[3] == '1' && bit_test_2[14] == '0' && bit_test_2[12] == '1' && bit_test_2[15] == '0' || bit_test_2[3] == '1' && bit_test_2[14] == '1' && bit_test_2[12] == '0' && bit_test_2[15] == '0' || bit_test_2[3] == '1' && bit_test_2[14] == '1' && bit_test_2[12] == '1' && bit_test_2[15] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 17:
		{
			if (bit_test_2[16] == '1' && bit_test_2[13] == '1' || bit_test_2[16] == '0' && bit_test_2[13] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 18:
		{
			if (bit_test_2[10] == '1' && bit_test_2[17] == '1' || bit_test_2[10] == '0' && bit_test_2[17] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 19:
		{
			if (bit_test_2[13] == '0' && bit_test_2[16] == '0' && bit_test_2[17] == '0' && bit_test_2[18] == '0' || bit_test_2[13] == '0' && bit_test_2[16] == '0' && bit_test_2[17] == '1' && bit_test_2[18] == '1' || bit_test_2[13] == '0' && bit_test_2[16] == '1' && bit_test_2[17] == '0' && bit_test_2[18] == '1' || bit_test_2[13] == '0' && bit_test_2[16] == '1' && bit_test_2[17] == '1' && bit_test_2[18] == '0' || bit_test_2[13] == '1' && bit_test_2[16] == '0' && bit_test_2[17] == '0' && bit_test_2[18] == '1' || bit_test_2[13] == '1' && bit_test_2[16] == '0' && bit_test_2[17] == '1' && bit_test_2[18] == '0' || bit_test_2[13] == '1' && bit_test_2[16] == '1' && bit_test_2[17] == '0' && bit_test_2[18] == '0' || bit_test_2[13] == '1' && bit_test_2[16] == '1' && bit_test_2[17] == '1' && bit_test_2[18] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 20:
		{
			if (bit_test_2[16] == '1' && bit_test_2[19] == '1' || bit_test_2[16] == '0' && bit_test_2[19] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 21:
		{
			if (bit_test_2[18] == '1' && bit_test_2[20] == '1' || bit_test_2[18] == '0' && bit_test_2[20] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 22:
		{
			if (bit_test_2[21] == '1' && bit_test_2[20] == '1' || bit_test_2[21] == '0' && bit_test_2[20] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 23:
		{
			if (bit_test_2[17] == '1' && bit_test_2[22] == '1' || bit_test_2[17] == '0' && bit_test_2[22] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 24:
		{
			if (bit_test_2[21] == '0' && bit_test_2[16] == '0' && bit_test_2[22] == '0' && bit_test_2[23] == '0' || bit_test_2[21] == '0' && bit_test_2[16] == '0' && bit_test_2[22] == '1' && bit_test_2[23] == '1' || bit_test_2[21] == '0' && bit_test_2[16] == '1' && bit_test_2[22] == '0' && bit_test_2[23] == '1' || bit_test_2[21] == '0' && bit_test_2[16] == '1' && bit_test_2[22] == '1' && bit_test_2[23] == '0' || bit_test_2[16] == '1' && bit_test_2[21] == '0' && bit_test_2[22] == '0' && bit_test_2[23] == '1' || bit_test_2[21] == '1' && bit_test_2[16] == '0' && bit_test_2[22] == '1' && bit_test_2[23] == '0' || bit_test_2[21] == '1' && bit_test_2[16] == '1' && bit_test_2[22] == '0' && bit_test_2[23] == '0' || bit_test_2[21] == '1' && bit_test_2[16] == '1' && bit_test_2[22] == '1' && bit_test_2[23] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 25:
		{
			if (bit_test_2[21] == '1' && bit_test_2[24] == '1' || bit_test_2[21] == '0' && bit_test_2[24] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 26:
		{
			if (bit_test_2[19] == '0' && bit_test_2[24] == '0' && bit_test_2[25] == '0' && bit_test_2[23] == '0' || bit_test_2[24] == '0' && bit_test_2[19] == '0' && bit_test_2[25] == '1' && bit_test_2[23] == '1' || bit_test_2[24] == '0' && bit_test_2[19] == '1' && bit_test_2[25] == '0' && bit_test_2[23] == '1' || bit_test_2[24] == '0' && bit_test_2[19] == '1' && bit_test_2[25] == '1' && bit_test_2[23] == '0' || bit_test_2[19] == '1' && bit_test_2[24] == '0' && bit_test_2[25] == '0' && bit_test_2[23] == '1' || bit_test_2[24] == '1' && bit_test_2[19] == '0' && bit_test_2[25] == '1' && bit_test_2[23] == '0' || bit_test_2[24] == '1' && bit_test_2[19] == '1' && bit_test_2[25] == '0' && bit_test_2[23] == '0' || bit_test_2[24] == '1' && bit_test_2[19] == '1' && bit_test_2[25] == '1' && bit_test_2[23] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 27:
		{
			if (bit_test_2[21] == '0' && bit_test_2[24] == '0' && bit_test_2[25] == '0' && bit_test_2[26] == '0' || bit_test_2[24] == '0' && bit_test_2[21] == '0' && bit_test_2[25] == '1' && bit_test_2[26] == '1' || bit_test_2[24] == '0' && bit_test_2[21] == '1' && bit_test_2[25] == '0' && bit_test_2[26] == '1' || bit_test_2[24] == '0' && bit_test_2[21] == '1' && bit_test_2[25] == '1' && bit_test_2[26] == '0' || bit_test_2[21] == '1' && bit_test_2[24] == '0' && bit_test_2[25] == '0' && bit_test_2[26] == '1' || bit_test_2[24] == '1' && bit_test_2[21] == '0' && bit_test_2[25] == '1' && bit_test_2[26] == '0' || bit_test_2[24] == '1' && bit_test_2[21] == '1' && bit_test_2[25] == '0' && bit_test_2[26] == '0' || bit_test_2[24] == '1' && bit_test_2[21] == '1' && bit_test_2[25] == '1' && bit_test_2[26] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 28:
		{
			if (bit_test_2[27] == '1' && bit_test_2[24] == '1' || bit_test_2[27] == '0' && bit_test_2[24] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 29:
		{
			if (bit_test_2[26] == '1' && bit_test_2[28] == '1' || bit_test_2[26] == '0' && bit_test_2[28] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 30:
		{
			if (bit_test_2[6] == '0' && bit_test_2[27] == '0' && bit_test_2[28] == '0' && bit_test_2[29] == '0' || bit_test_2[6] == '0' && bit_test_2[27] == '0' && bit_test_2[28] == '1' && bit_test_2[29] == '1' || bit_test_2[6] == '0' && bit_test_2[27] == '1' && bit_test_2[28] == '0' && bit_test_2[29] == '1' || bit_test_2[6] == '0' && bit_test_2[27] == '1' && bit_test_2[28] == '1' && bit_test_2[29] == '0' || bit_test_2[6] == '1' && bit_test_2[27] == '0' && bit_test_2[28] == '0' && bit_test_2[29] == '1' || bit_test_2[6] == '1' && bit_test_2[27] == '0' && bit_test_2[28] == '1' && bit_test_2[29] == '0' || bit_test_2[6] == '1' && bit_test_2[27] == '1' && bit_test_2[28] == '0' && bit_test_2[29] == '0' || bit_test_2[6] == '1' && bit_test_2[27] == '1' && bit_test_2[28] == '1' && bit_test_2[29] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 31:
		{
			if (bit_test_2[27] == '1' && bit_test_2[30] == '1' || bit_test_2[27] == '0' && bit_test_2[30] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 32:
		{
			if (bit_test_2[9] == '0' && bit_test_2[30] == '0' && bit_test_2[31] == '0' && bit_test_2[29] == '0' || bit_test_2[9] == '0' && bit_test_2[30] == '0' && bit_test_2[31] == '1' && bit_test_2[29] == '1' || bit_test_2[9] == '0' && bit_test_2[31] == '1' && bit_test_2[30] == '0' && bit_test_2[29] == '1' || bit_test_2[9] == '0' && bit_test_2[30] == '1' && bit_test_2[31] == '1' && bit_test_2[29] == '0' || bit_test_2[9] == '1' && bit_test_2[30] == '0' && bit_test_2[31] == '0' && bit_test_2[29] == '1' || bit_test_2[9] == '1' && bit_test_2[30] == '0' && bit_test_2[31] == '1' && bit_test_2[29] == '0' || bit_test_2[9] == '1' && bit_test_2[30] == '1' && bit_test_2[31] == '0' && bit_test_2[29] == '0' || bit_test_2[9] == '1' && bit_test_2[30] == '1' && bit_test_2[31] == '1' && bit_test_2[29] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 33:
		{
			if (bit_test_2[19] == '1' && bit_test_2[32] == '1' || bit_test_2[19] == '0' && bit_test_2[32] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 34:
		{
			if (bit_test_2[29] == '0' && bit_test_2[30] == '0' && bit_test_2[33] == '0' && bit_test_2[25] == '0' || bit_test_2[25] == '0' && bit_test_2[30] == '0' && bit_test_2[33] == '1' && bit_test_2[29] == '1' || bit_test_2[25] == '0' && bit_test_2[29] == '1' && bit_test_2[30] == '0' && bit_test_2[33] == '1' || bit_test_2[25] == '0' && bit_test_2[30] == '1' && bit_test_2[33] == '1' && bit_test_2[29] == '0' || bit_test_2[25] == '1' && bit_test_2[30] == '0' && bit_test_2[33] == '0' && bit_test_2[29] == '1' || bit_test_2[25] == '1' && bit_test_2[30] == '0' && bit_test_2[33] == '1' && bit_test_2[29] == '0' || bit_test_2[25] == '1' && bit_test_2[30] == '1' && bit_test_2[33] == '0' && bit_test_2[29] == '0' || bit_test_2[25] == '1' && bit_test_2[30] == '1' && bit_test_2[33] == '1' && bit_test_2[29] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 35:
		{
			if (bit_test_2[34] == '1' && bit_test_2[32] == '1' || bit_test_2[34] == '0' && bit_test_2[32] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 36:
		{
			if (bit_test_2[24] == '1' && bit_test_2[35] == '1' || bit_test_2[24] == '0' && bit_test_2[35] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 37:
		{
			if (bit_test_2[32] == '0' && bit_test_2[30] == '0' && bit_test_2[36] == '0' && bit_test_2[35] == '0' || bit_test_2[30] == '0' && bit_test_2[32] == '0' && bit_test_2[35] == '1' && bit_test_2[36] == '1' || bit_test_2[30] == '0' && bit_test_2[32] == '1' && bit_test_2[35] == '0' && bit_test_2[36] == '1' || bit_test_2[30] == '0' && bit_test_2[32] == '1' && bit_test_2[35] == '1' && bit_test_2[36] == '0' || bit_test_2[30] == '1' && bit_test_2[32] == '0' && bit_test_2[35] == '0' && bit_test_2[36] == '1' || bit_test_2[30] == '1' && bit_test_2[32] == '0' && bit_test_2[35] == '1' && bit_test_2[36] == '0' || bit_test_2[30] == '1' && bit_test_2[32] == '1' && bit_test_2[35] == '0' && bit_test_2[36] == '0' || bit_test_2[30] == '1' && bit_test_2[32] == '1' && bit_test_2[35] == '1' && bit_test_2[36] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 38:
		{
			if (bit_test_2[32] == '0' && bit_test_2[31] == '0' && bit_test_2[36] == '0' && bit_test_2[37] == '0' || bit_test_2[31] == '0' && bit_test_2[32] == '0' && bit_test_2[37] == '1' && bit_test_2[36] == '1' || bit_test_2[31] == '0' && bit_test_2[32] == '1' && bit_test_2[37] == '0' && bit_test_2[36] == '1' || bit_test_2[31] == '0' && bit_test_2[32] == '1' && bit_test_2[37] == '1' && bit_test_2[36] == '0' || bit_test_2[31] == '1' && bit_test_2[32] == '0' && bit_test_2[37] == '0' && bit_test_2[36] == '1' || bit_test_2[31] == '1' && bit_test_2[32] == '0' && bit_test_2[37] == '1' && bit_test_2[36] == '0' || bit_test_2[31] == '1' && bit_test_2[32] == '1' && bit_test_2[37] == '0' && bit_test_2[36] == '0' || bit_test_2[31] == '1' && bit_test_2[32] == '1' && bit_test_2[37] == '1' && bit_test_2[36] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 39:
		{
			if (bit_test_2[34] == '1' && bit_test_2[38] == '1' || bit_test_2[34] == '0' && bit_test_2[38] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 40:
		{
			if (bit_test_2[34] == '0' && bit_test_2[35] == '0' && bit_test_2[36] == '0' && bit_test_2[39] == '0' || bit_test_2[34] == '0' && bit_test_2[35] == '0' && bit_test_2[39] == '1' && bit_test_2[36] == '1' || bit_test_2[34] == '0' && bit_test_2[35] == '1' && bit_test_2[39] == '0' && bit_test_2[36] == '1' || bit_test_2[34] == '0' && bit_test_2[35] == '1' && bit_test_2[39] == '1' && bit_test_2[36] == '0' || bit_test_2[34] == '1' && bit_test_2[35] == '0' && bit_test_2[39] == '0' && bit_test_2[36] == '1' || bit_test_2[34] == '1' && bit_test_2[35] == '0' && bit_test_2[39] == '1' && bit_test_2[36] == '0' || bit_test_2[34] == '1' && bit_test_2[35] == '1' && bit_test_2[39] == '0' && bit_test_2[36] == '0' || bit_test_2[34] == '1' && bit_test_2[35] == '1' && bit_test_2[39] == '1' && bit_test_2[36] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 41:
		{
			if (bit_test_2[37] == '1' && bit_test_2[40] == '1' || bit_test_2[37] == '0' && bit_test_2[40] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 42:
		{
			if (bit_test_2[34] == '0' && bit_test_2[37] == '0' && bit_test_2[38] == '0' && bit_test_2[41] == '0' || bit_test_2[34] == '0' && bit_test_2[37] == '0' && bit_test_2[38] == '1' && bit_test_2[41] == '1' || bit_test_2[34] == '0' && bit_test_2[37] == '1' && bit_test_2[38] == '0' && bit_test_2[41] == '1' || bit_test_2[34] == '0' && bit_test_2[37] == '1' && bit_test_2[38] == '1' && bit_test_2[41] == '0' || bit_test_2[34] == '1' && bit_test_2[37] == '0' && bit_test_2[38] == '0' && bit_test_2[41] == '1' || bit_test_2[34] == '1' && bit_test_2[37] == '0' && bit_test_2[38] == '1' && bit_test_2[41] == '0' || bit_test_2[34] == '1' && bit_test_2[37] == '1' && bit_test_2[38] == '0' && bit_test_2[41] == '0' || bit_test_2[34] == '1' && bit_test_2[37] == '1' && bit_test_2[38] == '1' && bit_test_2[41] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 43:
		{
			if (bit_test_2[36] == '0' && bit_test_2[39] == '0' && bit_test_2[38] == '0' && bit_test_2[42] == '0' || bit_test_2[36] == '0' && bit_test_2[39] == '0' && bit_test_2[38] == '1' && bit_test_2[42] == '1' || bit_test_2[36] == '0' && bit_test_2[39] == '1' && bit_test_2[38] == '0' && bit_test_2[42] == '1' || bit_test_2[36] == '0' && bit_test_2[39] == '1' && bit_test_2[38] == '1' && bit_test_2[42] == '0' || bit_test_2[36] == '1' && bit_test_2[39] == '0' && bit_test_2[38] == '0' && bit_test_2[42] == '1' || bit_test_2[36] == '1' && bit_test_2[39] == '0' && bit_test_2[38] == '1' && bit_test_2[42] == '0' || bit_test_2[36] == '1' && bit_test_2[39] == '1' && bit_test_2[38] == '0' && bit_test_2[42] == '0' || bit_test_2[36] == '1' && bit_test_2[39] == '1' && bit_test_2[38] == '1' && bit_test_2[42] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 44:
		{
			if (bit_test_2[43] == '0' && bit_test_2[37] == '0' && bit_test_2[38] == '0' && bit_test_2[41] == '0' || bit_test_2[43] == '0' && bit_test_2[37] == '0' && bit_test_2[38] == '1' && bit_test_2[41] == '1' || bit_test_2[43] == '0' && bit_test_2[37] == '1' && bit_test_2[38] == '0' && bit_test_2[41] == '1' || bit_test_2[43] == '0' && bit_test_2[37] == '1' && bit_test_2[38] == '1' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[37] == '0' && bit_test_2[38] == '0' && bit_test_2[41] == '1' || bit_test_2[43] == '1' && bit_test_2[37] == '0' && bit_test_2[38] == '1' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[37] == '1' && bit_test_2[38] == '0' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[37] == '1' && bit_test_2[38] == '1' && bit_test_2[41] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 45:
		{
			if (bit_test_2[43] == '0' && bit_test_2[40] == '0' && bit_test_2[44] == '0' && bit_test_2[41] == '0' || bit_test_2[43] == '0' && bit_test_2[40] == '0' && bit_test_2[44] == '1' && bit_test_2[41] == '1' || bit_test_2[43] == '0' && bit_test_2[40] == '1' && bit_test_2[44] == '0' && bit_test_2[41] == '1' || bit_test_2[43] == '0' && bit_test_2[40] == '1' && bit_test_2[44] == '1' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '0' && bit_test_2[44] == '0' && bit_test_2[41] == '1' || bit_test_2[43] == '1' && bit_test_2[40] == '0' && bit_test_2[44] == '1' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '1' && bit_test_2[44] == '0' && bit_test_2[41] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '1' && bit_test_2[44] == '1' && bit_test_2[41] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 46:
		{
			if (bit_test_2[37] == '0' && bit_test_2[39] == '0' && bit_test_2[38] == '0' && bit_test_2[45] == '0' || bit_test_2[37] == '0' && bit_test_2[39] == '0' && bit_test_2[38] == '1' && bit_test_2[45] == '1' || bit_test_2[37] == '0' && bit_test_2[39] == '1' && bit_test_2[38] == '0' && bit_test_2[45] == '1' || bit_test_2[37] == '0' && bit_test_2[39] == '1' && bit_test_2[38] == '1' && bit_test_2[45] == '0' || bit_test_2[37] == '1' && bit_test_2[39] == '0' && bit_test_2[38] == '0' && bit_test_2[45] == '1' || bit_test_2[37] == '1' && bit_test_2[39] == '0' && bit_test_2[38] == '1' && bit_test_2[45] == '0' || bit_test_2[37] == '1' && bit_test_2[39] == '1' && bit_test_2[38] == '0' && bit_test_2[45] == '0' || bit_test_2[37] == '1' && bit_test_2[39] == '1' && bit_test_2[38] == '1' && bit_test_2[45] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 47:
		{
			if (bit_test_2[41] == '1' && bit_test_2[46] == '1' || bit_test_2[41] == '0' && bit_test_2[46] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 48:
		{
			if (bit_test_2[43] == '0' && bit_test_2[40] == '0' && bit_test_2[38] == '0' && bit_test_2[47] == '0' || bit_test_2[43] == '0' && bit_test_2[40] == '0' && bit_test_2[38] == '1' && bit_test_2[47] == '1' || bit_test_2[43] == '0' && bit_test_2[40] == '1' && bit_test_2[38] == '0' && bit_test_2[47] == '1' || bit_test_2[43] == '0' && bit_test_2[40] == '1' && bit_test_2[38] == '1' && bit_test_2[47] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '0' && bit_test_2[38] == '0' && bit_test_2[47] == '1' || bit_test_2[43] == '1' && bit_test_2[40] == '0' && bit_test_2[38] == '1' && bit_test_2[47] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '1' && bit_test_2[38] == '0' && bit_test_2[47] == '0' || bit_test_2[43] == '1' && bit_test_2[40] == '1' && bit_test_2[38] == '1' && bit_test_2[47] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 49:
		{
			if (bit_test_2[39] == '1' && bit_test_2[48] == '1' || bit_test_2[39] == '0' && bit_test_2[48] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 50:
		{
			if (bit_test_2[45] == '0' && bit_test_2[46] == '0' && bit_test_2[47] == '0' && bit_test_2[49] == '0' || bit_test_2[45] == '0' && bit_test_2[46] == '0' && bit_test_2[47] == '1' && bit_test_2[49] == '1' || bit_test_2[45] == '0' && bit_test_2[46] == '1' && bit_test_2[47] == '0' && bit_test_2[49] == '1' || bit_test_2[45] == '0' && bit_test_2[46] == '1' && bit_test_2[47] == '1' && bit_test_2[49] == '0' || bit_test_2[45] == '1' && bit_test_2[46] == '0' && bit_test_2[47] == '0' && bit_test_2[49] == '1' || bit_test_2[45] == '1' && bit_test_2[46] == '0' && bit_test_2[47] == '1' && bit_test_2[49] == '0' || bit_test_2[45] == '1' && bit_test_2[46] == '1' && bit_test_2[47] == '0' && bit_test_2[49] == '0' || bit_test_2[45] == '1' && bit_test_2[46] == '1' && bit_test_2[47] == '1' && bit_test_2[49] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 51:
		{
			if (bit_test_2[44] == '0' && bit_test_2[50] == '0' && bit_test_2[47] == '0' && bit_test_2[49] == '0' || bit_test_2[44] == '0' && bit_test_2[50] == '0' && bit_test_2[47] == '1' && bit_test_2[49] == '1' || bit_test_2[44] == '0' && bit_test_2[50] == '1' && bit_test_2[47] == '0' && bit_test_2[49] == '1' || bit_test_2[44] == '0' && bit_test_2[50] == '1' && bit_test_2[47] == '1' && bit_test_2[49] == '0' || bit_test_2[44] == '1' && bit_test_2[50] == '0' && bit_test_2[47] == '0' && bit_test_2[49] == '1' || bit_test_2[44] == '1' && bit_test_2[50] == '0' && bit_test_2[47] == '1' && bit_test_2[49] == '0' || bit_test_2[44] == '1' && bit_test_2[50] == '1' && bit_test_2[47] == '0' && bit_test_2[49] == '0' || bit_test_2[44] == '1' && bit_test_2[50] == '1' && bit_test_2[47] == '1' && bit_test_2[49] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 52:
		{
			if (bit_test_2[51] == '1' && bit_test_2[48] == '1' || bit_test_2[51] == '0' && bit_test_2[48] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 53:
		{
			if (bit_test_2[50] == '0' && bit_test_2[46] == '0' && bit_test_2[51] == '0' && bit_test_2[52] == '0' || bit_test_2[50] == '0' && bit_test_2[46] == '0' && bit_test_2[51] == '1' && bit_test_2[52] == '1' || bit_test_2[50] == '0' && bit_test_2[46] == '1' && bit_test_2[51] == '0' && bit_test_2[52] == '1' || bit_test_2[50] == '0' && bit_test_2[46] == '1' && bit_test_2[51] == '1' && bit_test_2[52] == '0' || bit_test_2[50] == '1' && bit_test_2[46] == '0' && bit_test_2[51] == '0' && bit_test_2[52] == '1' || bit_test_2[50] == '1' && bit_test_2[46] == '0' && bit_test_2[51] == '1' && bit_test_2[52] == '0' || bit_test_2[50] == '1' && bit_test_2[46] == '1' && bit_test_2[51] == '0' && bit_test_2[52] == '0' || bit_test_2[50] == '1' && bit_test_2[46] == '1' && bit_test_2[51] == '1' && bit_test_2[52] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 54:
		{
			if (bit_test_2[45] == '0' && bit_test_2[50] == '0' && bit_test_2[47] == '0' && bit_test_2[53] == '0' || bit_test_2[45] == '0' && bit_test_2[50] == '0' && bit_test_2[47] == '1' && bit_test_2[53] == '1' || bit_test_2[45] == '0' && bit_test_2[50] == '1' && bit_test_2[47] == '0' && bit_test_2[53] == '1' || bit_test_2[45] == '0' && bit_test_2[50] == '1' && bit_test_2[47] == '1' && bit_test_2[53] == '0' || bit_test_2[45] == '1' && bit_test_2[50] == '0' && bit_test_2[47] == '0' && bit_test_2[53] == '1' || bit_test_2[45] == '1' && bit_test_2[50] == '0' && bit_test_2[47] == '1' && bit_test_2[53] == '0' || bit_test_2[45] == '1' && bit_test_2[50] == '1' && bit_test_2[47] == '0' && bit_test_2[53] == '0' || bit_test_2[45] == '1' && bit_test_2[50] == '1' && bit_test_2[47] == '1' && bit_test_2[53] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 55:
		{
			if (bit_test_2[54] == '1' && bit_test_2[30] == '1' || bit_test_2[54] == '0' && bit_test_2[30] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 56:
		{
			if (bit_test_2[51] == '0' && bit_test_2[48] == '0' && bit_test_2[53] == '0' && bit_test_2[55] == '0' || bit_test_2[51] == '0' && bit_test_2[48] == '0' && bit_test_2[53] == '1' && bit_test_2[55] == '1' || bit_test_2[51] == '0' && bit_test_2[48] == '1' && bit_test_2[53] == '0' && bit_test_2[55] == '1' || bit_test_2[51] == '0' && bit_test_2[48] == '1' && bit_test_2[53] == '1' && bit_test_2[55] == '0' || bit_test_2[51] == '1' && bit_test_2[48] == '0' && bit_test_2[53] == '0' && bit_test_2[55] == '1' || bit_test_2[51] == '1' && bit_test_2[48] == '0' && bit_test_2[53] == '1' && bit_test_2[55] == '0' || bit_test_2[51] == '1' && bit_test_2[48] == '1' && bit_test_2[53] == '0' && bit_test_2[55] == '0' || bit_test_2[51] == '1' && bit_test_2[48] == '1' && bit_test_2[53] == '1' && bit_test_2[55] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 57:
		{
			if (bit_test_2[56] == '1' && bit_test_2[49] == '1' || bit_test_2[56] == '0' && bit_test_2[49] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 58:
		{
			if (bit_test_2[57] == '1' && bit_test_2[38] == '1' || bit_test_2[57] == '0' && bit_test_2[38] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 59:
		{
			if (bit_test_2[54] == '0' && bit_test_2[56] == '0' && bit_test_2[51] == '0' && bit_test_2[58] == '0' || bit_test_2[54] == '0' && bit_test_2[56] == '0' && bit_test_2[51] == '1' && bit_test_2[58] == '1' || bit_test_2[54] == '0' && bit_test_2[56] == '1' && bit_test_2[51] == '0' && bit_test_2[58] == '1' || bit_test_2[54] == '0' && bit_test_2[56] == '1' && bit_test_2[51] == '1' && bit_test_2[58] == '0' || bit_test_2[54] == '1' && bit_test_2[56] == '0' && bit_test_2[51] == '0' && bit_test_2[58] == '1' || bit_test_2[54] == '1' && bit_test_2[56] == '0' && bit_test_2[51] == '1' && bit_test_2[58] == '0' || bit_test_2[54] == '1' && bit_test_2[56] == '1' && bit_test_2[51] == '0' && bit_test_2[58] == '0' || bit_test_2[54] == '1' && bit_test_2[56] == '1' && bit_test_2[51] == '1' && bit_test_2[58] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 60:
		{
			if (bit_test_2[59] == '1' && bit_test_2[58] == '1' || bit_test_2[59] == '0' && bit_test_2[58] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 61:
		{
			if (bit_test_2[55] == '0' && bit_test_2[59] == '0' && bit_test_2[60] == '0' && bit_test_2[58] == '0' || bit_test_2[55] == '0' && bit_test_2[59] == '0' && bit_test_2[60] == '1' && bit_test_2[58] == '1' || bit_test_2[55] == '0' && bit_test_2[59] == '1' && bit_test_2[60] == '0' && bit_test_2[58] == '1' || bit_test_2[55] == '0' && bit_test_2[59] == '1' && bit_test_2[60] == '1' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[59] == '0' && bit_test_2[60] == '0' && bit_test_2[58] == '1' || bit_test_2[55] == '1' && bit_test_2[59] == '0' && bit_test_2[60] == '1' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[59] == '1' && bit_test_2[60] == '0' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[59] == '1' && bit_test_2[60] == '1' && bit_test_2[58] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 62:
		{
			if (bit_test_2[55] == '0' && bit_test_2[56] == '0' && bit_test_2[60] == '0' && bit_test_2[58] == '0' || bit_test_2[55] == '0' && bit_test_2[56] == '0' && bit_test_2[60] == '1' && bit_test_2[58] == '1' || bit_test_2[55] == '0' && bit_test_2[56] == '1' && bit_test_2[60] == '0' && bit_test_2[58] == '1' || bit_test_2[55] == '0' && bit_test_2[56] == '1' && bit_test_2[60] == '1' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[56] == '0' && bit_test_2[60] == '0' && bit_test_2[58] == '1' || bit_test_2[55] == '1' && bit_test_2[56] == '0' && bit_test_2[60] == '1' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[56] == '1' && bit_test_2[60] == '0' && bit_test_2[58] == '0' || bit_test_2[55] == '1' && bit_test_2[56] == '1' && bit_test_2[60] == '1' && bit_test_2[58] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 63:
		{
			if (bit_test_2[61] == '1' && bit_test_2[62] == '1' || bit_test_2[61] == '0' && bit_test_2[62] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 64:
		{
			if (bit_test_2[62] == '0' && bit_test_2[59] == '0' && bit_test_2[60] == '0' && bit_test_2[63] == '0' || bit_test_2[62] == '0' && bit_test_2[59] == '0' && bit_test_2[60] == '1' && bit_test_2[63] == '1' || bit_test_2[62] == '0' && bit_test_2[59] == '1' && bit_test_2[60] == '0' && bit_test_2[63] == '1' || bit_test_2[62] == '0' && bit_test_2[59] == '1' && bit_test_2[60] == '1' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[59] == '0' && bit_test_2[60] == '0' && bit_test_2[63] == '1' || bit_test_2[62] == '1' && bit_test_2[59] == '0' && bit_test_2[60] == '1' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[59] == '1' && bit_test_2[60] == '0' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[59] == '1' && bit_test_2[60] == '1' && bit_test_2[63] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 65:
		{
			if (bit_test_2[64] == '1' && bit_test_2[46] == '1' || bit_test_2[64] == '0' && bit_test_2[46] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 66:
		{
			if (bit_test_2[56] == '0' && bit_test_2[59] == '0' && bit_test_2[65] == '0' && bit_test_2[57] == '0' || bit_test_2[56] == '0' && bit_test_2[59] == '0' && bit_test_2[65] == '1' && bit_test_2[57] == '1' || bit_test_2[56] == '0' && bit_test_2[59] == '1' && bit_test_2[65] == '0' && bit_test_2[57] == '1' || bit_test_2[56] == '0' && bit_test_2[59] == '1' && bit_test_2[65] == '1' && bit_test_2[57] == '0' || bit_test_2[56] == '1' && bit_test_2[59] == '0' && bit_test_2[65] == '0' && bit_test_2[57] == '1' || bit_test_2[56] == '1' && bit_test_2[59] == '0' && bit_test_2[65] == '1' && bit_test_2[57] == '0' || bit_test_2[56] == '1' && bit_test_2[59] == '1' && bit_test_2[65] == '0' && bit_test_2[57] == '0' || bit_test_2[56] == '1' && bit_test_2[59] == '1' && bit_test_2[65] == '1' && bit_test_2[57] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 67:
		{
			if (bit_test_2[61] == '0' && bit_test_2[64] == '0' && bit_test_2[65] == '0' && bit_test_2[66] == '0' || bit_test_2[61] == '0' && bit_test_2[64] == '0' && bit_test_2[65] == '1' && bit_test_2[66] == '1' || bit_test_2[61] == '0' && bit_test_2[64] == '1' && bit_test_2[65] == '0' && bit_test_2[66] == '1' || bit_test_2[61] == '0' && bit_test_2[64] == '1' && bit_test_2[65] == '1' && bit_test_2[66] == '0' || bit_test_2[61] == '1' && bit_test_2[64] == '0' && bit_test_2[65] == '0' && bit_test_2[66] == '1' || bit_test_2[61] == '1' && bit_test_2[64] == '0' && bit_test_2[65] == '1' && bit_test_2[66] == '0' || bit_test_2[61] == '1' && bit_test_2[64] == '1' && bit_test_2[65] == '0' && bit_test_2[66] == '0' || bit_test_2[61] == '1' && bit_test_2[64] == '1' && bit_test_2[65] == '1' && bit_test_2[66] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 68:
		{
			if (bit_test_2[67] == '1' && bit_test_2[58] == '1' || bit_test_2[67] == '0' && bit_test_2[58] == '0') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 69:
		{
			if (bit_test_2[62] == '0' && bit_test_2[66] == '0' && bit_test_2[68] == '0' && bit_test_2[63] == '0' || bit_test_2[62] == '0' && bit_test_2[66] == '0' && bit_test_2[68] == '1' && bit_test_2[63] == '1' || bit_test_2[62] == '0' && bit_test_2[66] == '1' && bit_test_2[68] == '0' && bit_test_2[63] == '1' || bit_test_2[62] == '0' && bit_test_2[66] == '1' && bit_test_2[68] == '1' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[66] == '0' && bit_test_2[68] == '0' && bit_test_2[63] == '1' || bit_test_2[62] == '1' && bit_test_2[66] == '0' && bit_test_2[68] == '1' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[66] == '1' && bit_test_2[68] == '0' && bit_test_2[63] == '0' || bit_test_2[62] == '1' && bit_test_2[66] == '1' && bit_test_2[68] == '1' && bit_test_2[63] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		case 70:
		{
			if (bit_test_2[68] == '0' && bit_test_2[64] == '0' && bit_test_2[69] == '0' && bit_test_2[66] == '0' || bit_test_2[68] == '0' && bit_test_2[64] == '0' && bit_test_2[69] == '1' && bit_test_2[66] == '1' || bit_test_2[68] == '0' && bit_test_2[64] == '1' && bit_test_2[69] == '0' && bit_test_2[66] == '1' || bit_test_2[68] == '0' && bit_test_2[64] == '1' && bit_test_2[69] == '1' && bit_test_2[66] == '0' || bit_test_2[68] == '1' && bit_test_2[64] == '0' && bit_test_2[69] == '0' && bit_test_2[66] == '1' || bit_test_2[68] == '1' && bit_test_2[64] == '0' && bit_test_2[69] == '1' && bit_test_2[66] == '0' || bit_test_2[68] == '1' && bit_test_2[64] == '1' && bit_test_2[69] == '0' && bit_test_2[66] == '0' || bit_test_2[68] == '1' && bit_test_2[64] == '1' && bit_test_2[69] == '1' && bit_test_2[66] == '1') {
				null_element2 = '0';
			}
			else {
				null_element2 = '1';
			}
			rotate(bit_test_2.rbegin(), bit_test_2.rbegin() + 1, bit_test_2.rend());
			bit_test_2[0] = null_element2;
			break;
		}

		//add more number
		}


		register int name_test;
		name_test = bit_test_1[0] ^ bit_test_2[0];
		ss << name_test;
		ss >> beatstream_bit;

		beatstream += beatstream_bit;


		if (size(beatstream) == size(bit_test_3)) {
			cout << "\nБитовый поток для расшифрования: " << beatstream << "\n";
		}

	}

	//расшифровка слова
	register char element;
	register string encrypted_word = "";
	register int counter1 = 0;

	for (register int counter_i = 0; counter_i < size(bit_test_3); counter_i++) {

		if (beatstream[counter_i] == '1' && bit_test_3[counter_i] == '1' || beatstream[counter_i] == '0' && bit_test_3[counter_i] == '0') {
			encrypted_word += "0";
		}
		else {
			encrypted_word += "1";
		}

	}

	cout << "\nБитовый поток зашифрованного слова: " << encrypted_word << "\n";

	register int beging = 0;
	register string c = "";
	register int decimal_Number_int;
	register string a = "";

	register string bit_number = "";
	register string encrypted_total_str = "";

	//bit_3_encrypt_1(x2);
	for (register int counter_str = 0; counter_str <= size(bit_test_3); counter_str++) {

		register string a = encrypted_word.substr(0 + counter_str * 7, 7);
		c += a;
		decimal_Number_int = decimal_Number(a, counter_str);

		bit_number = (char)(decimal_Number_int);

		encrypted_total_str += bit_number;

		cout << "\nЭлементы бинарного потока:  " << a << "   Десятичное число элемента: " << decimal_Number_int << "   Элемент зашифрованного слова: " << bit_number << "\n";

		if ((size(bit_test_3) - size(c)) < 7) {

			cout << "\nГотово.   Зашифрованное слово: " << encrypted_total_str << "\n";
			goto end;
		}
	}

end:

	return "1";
}


int main()
{
	setlocale(LC_ALL, "Russian");
	cout << "Первое задание: \n";
	register string choice;


	cout << "\nЕсли вы хотите зашифровать поток информации, то введите '1'. Если вы расшифровать поток информации, то введите '0' \n";

	while (choice != "1" || choice != "0") {
		cin >> choice;
		if (choice == "1") {
			register string bitstream = encrypt_1();

			break;
		}
		if (choice == "0") {
			register string decrypt_bitstream = decrypt_1();

			break;
		}
		else {
			cout << "Попробуйте ещё раз: ";
		}
	}

}
